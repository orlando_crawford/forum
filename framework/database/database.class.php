<?php
  require_once(__DIR__ . '\../lib/util.php');
  require_once(__DIR__ . '\../core/cache.class.php');
  class c_cache_db_common_bindings{
    const SUB_CACHE = "_db_cache_sub:";
    const USER_CACHE = "_db_cache_user:";
    const SUB_THREAD_PAGE_CACHE = "_db_cache_sub_fpage:";
  }
  class enum_DB_RAND_SORT{
    const RANDOM_THREAD = 0;
    const RANDOM_SUB = 1;
    const RANDOM_COMMENT = 3; //not implimented
  }
  class c_db {
    const CONNECTED = 0;
    const QUERY_UNEXPECTED_RESULT = -1;
    const QUERY_BAD_ARGUMENT = -2;
    const QUERY_FAILED = -3;
    const NO_CONNECTION = -4;
    private static $instance = NULL;
    private static $error = 0;
    private $_result;
    function __construct() {}
    private function __clone() {}
    public static function connected(){
      return self::getInstance() != NULL;
    }
    public function _set_result($val){
      $this->_result = $val;
    }
    public function get_result(){
      return $this->_result;
    }
    public static function set_error($val){
      return self::$error = $val;
    }
    public static function get_error(){
      return self::$error;
    }
    public static function getInstance() {
      if (!isset(self::$instance)) {
        try{
          $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
          self::$instance = new PDO('mysql:host=localhost:3305;dbname=forum', 'root', 'password', $pdo_options);
        } catch(Exception $e){
          //echo $e;
          self::set_error(self::NO_CONNECTION);
          return self::$instance = NULL;
        }
      }
      self::set_error(self::CONNECTED);
      return self::$instance;
    }
    public function execute_prepared(string $query, array $var_args){
      //if(!self::connected())
      //  return self::$error;
      try{
        $con = self::getInstance();
        if($con == NULL)
          return false;
        $stmt = $con->prepare($query);
        // /self::getInstance()->set_charset('utf8');
        if($stmt == false){
          $this->set_error(self::QUERY_FAILED);
          return false;
        }
        $ret = $stmt->execute($var_args);
        if($ret == false){
          $this->set_error(self::QUERY_FAILED);
          return false;
        }
        //echo "Ret: [" . $ret . "]";
        return $stmt;
        //$result = $stmt->fetch();
        //if($result == false)
        //  return $this->set_error(self::QUERY_FAILED);
      }catch(Exception $e){
        $this->set_error(self::QUERY_FAILED);
        return false;
      }
      //if(!is_array($result))return false;
      //if(count($result) != ($expected_num_result + 1)) return self::QUERY_UNEXPECTED_RESULT;
      return false;
    }
  }

  class c_timer{
    private $time_start = 0;
    private $time_end = 0;
    function _time_ms(){
      return round(microtime(true) * 1000);
    }
    function start(){
      $this->time_start = $this->_time_ms();
    }
    function stop(){
      $this->time_stop = $this->_time_ms();
    }
    function get_elapsed_ms_now() : int{
      return ($this->_time_ms() - $this->time_start);
    }
    function get_elapsed_sec() : int{
      return $this->get_elapsed_ms() / 60;
    }
    function get_elapsed_ms(){
      return ($this->time_stop - $this->time_start);
    }
  }
  class c_db_object{
    function type() : int {}
    function by_id() : int {}
    function by_b32(): string {}
    function by_name() : string{}
  }
  class c_thread_sorting{
    const NEW = 0;
    const TOP = 1;
    const RISING = 2;
    const CONTROVERSIAL = 3;
  }
  class c_db_sub_connection extends c_db{
    function query_sub_id(string $str_name) : int{
      /*!class function description*/
      if(!is_string($str_name))
        return self::QUERY_BAD_ARGUMENT;
      $cache = new c_memcache_controller();
      $cache->bad();

      $str_subname = strtolower($str_name);
      $cache_key = c_cache_db_common_bindings::SUB_CACHE . $str_subname;
      //Check the memory cache to see if the object exists,
      //if it does, return true,
      //else query db and set memcache
      $handler = new c_memcache_common_handler();
      $timer = new c_timer();
      $timer->start();
      if(false && $handler->object_exists($cache, $cache_key)){
        $val = $handler->object_get($cache, $cache_key);
        $timer->stop();
        dlog()->line(3, "c_db_sub_connection Cache hit: " . $timer->get_elapsed_ms() . "ms");
        return $val;
      }
      $timer->stop();
      dlog()->line(3, "c_db_sub_connection Cache miss: " . $timer->get_elapsed_ms() . "ms");

      $timer->start();
      //$handler->object_get($cache, $key);
      if(self::getInstance() == NULL)
        return $this->get_error();
      $stmt = self::getInstance()->prepare('SELECT sub_exists(?)');
      if($stmt == false)
      return self::QUERY_FAILED;
      $stmt->execute([$str_subname]);
      if(!$stmt)
        return self::QUERY_FAILED;
      $result = $stmt->fetch();
      if($result == false)
        return self::QUERY_FAILED;
      if(count($result) != 2)
        return self::QUERY_UNEXPECTED_RESULT;
      $timer->stop();
      dlog()->line(3, "c_db_sub_connection db hit: " . $timer->get_elapsed_ms() . "ms");

      if($result[0] == '-1'){
        echo "result bad: ";
        $handler->set($cache, $cache_key, false);
        return 0;
      }else{// if($result[0] == '1'){
        $handler->set($cache, $cache_key, true);
        return $result[0];
        //return self::QUERY_UNEXPECTED_RESULT;
      }
    }
    function query_sub_exist(string $str_name) : int{
      $ret = $this->query_sub_id($str_name);
      if($ret < 0)
        return 0;
      else
        return 1;
      //$exists = ($result[0] == '0' ? false : $result[0] == '1' ? 1 : false; //self::QUERY_UNEXPECTED_RESULT;;
      //if($exists){
        //$ret = $handler->set($cache, "_db_cache_sub:" . $str_site);
        //return 0;
      //}
      //decho(print_r($result));    //print array
      //decho(count($result));      //print size
      //decho(gettype($result[0])); //print type at index 0
    }
    function query_sub_page_exists() : int{
      return 0;
    }
    function query_sub_info_by_name(string $str_name) : int{
      $str_subname = strtolower($str_name);
      if($this->query_sub_exist($str_subname) == 0){
        dlog()->line(3, "sub does not exist");
        return -1;
      }
      query_sub_info(0);
      return 0;
    }
    function query_sub_info(int $index){
      if(self::getInstance() == NULL)
        return $this->get_error();
      $stmt = $this->execute_prepared('CALL p_sub_info(?)', [$index]);
      if($stmt == false)
        return false;
      $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
      if($result == false)
        return self::QUERY_FAILED;
      if(count($result) < 1 || !is_array($result[0]))
        return self::QUERY_UNEXPECTED_RESULT;
      //if(count($result) < 3)
      //  return self::QUERY_UNEXPECTED_RESULT;
      return $result[0];
    }
    function query_sub_count(){
      if(self::getInstance() == NULL)
        return $this->get_error();
      $stmt = self::getInstance()->prepare('CALL sub_count()');
      if($stmt == false)
        return self::QUERY_FAILED;
      $stmt->execute([]);
      if(!$stmt)
        return self::QUERY_FAILED;
      $result = $stmt->fetch();
      if($result == false)
        return self::QUERY_FAILED;
      if(count($result) != 2)
        return self::QUERY_UNEXPECTED_RESULT;
      print_r($result);
      return $result;
    }
    function query_rand_thing(int $int_sorting){
      switch ($int_sorting){
        case enum_DB_RAND_SORT::RANDOM_THREAD:
          $stmt = $this->execute_prepared("
            CALL rand_thread(?)", [0]);
          break;
      }
      if($stmt == false){
        $this->set_error(self::QUERY_FAILED);
        return false;
      }
      $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
      if($result == false){
        $this->set_error(self::QUERY_FAILED);
        return false;
      }
    }
    function query_sub_list(int $int_clamp){
      $stmt = $this->execute_prepared("
        SELECT
          sub.name,
          sub.text_description,
          sub.access_level
        FROM
          sub
        WHERE is_deleted = FALSE;", []);
      if($stmt == false){
        $this->set_error(self::QUERY_FAILED);
        return false;
      }
      $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
      if($result == false){
        $this->set_error(self::QUERY_FAILED);
        return false;
      }
      if(!is_array($result))
        return false;
      return $result;
    }
    function query_sub_create(string $str_name) : int{
      dlog()->line(3, "query_sub_create");
      $str_subname = strtolower($str_name);
      if($this->query_sub_exist($str_subname) != 0){
        dlog()->line(3, "sub already exists");
        return -1;
      }
      $user_id = 1;//session()->user()->get_user_id();
      dlog()->line(3, "query_sub_create -> inserting");
      if(self::getInstance() == NULL)
        return $this->get_error();
      $stmt = self::getInstance()->prepare('SELECT sub_create(?, ?)');
      if($stmt == false)
        return self::QUERY_FAILED;
      $stmt->execute([$str_name, $user_id]);
      if(!$stmt)
        return self::QUERY_FAILED;
      $result = $stmt->fetch();
        if($result == false)
        return self::QUERY_FAILED;
      if(count($result) != 2)
        return self::QUERY_UNEXPECTED_RESULT;

      $cache = new c_memcache_controller();
      $cache_key = c_cache_db_common_bindings::SUB_CACHE . $str_subname;
      $handler = new c_memcache_common_handler();
      //$handler->set($cache, $cache_key, true);
      return 0;
    }
    function query_sub_id_by_name(string $str_name) : int{
      return 1;
    }
    function query_comment_insert(
      int $int_user_id,
      int $int_thread_id,
      int $int_comment_parent_id,
      string $str_text){
      /*
      param_thread_id int,
	     user_author_id int unsigned,
       comment_parent_id INT,
      str_text string
      */
      //$_uid = 1;
      $stmt = $this->execute_prepared(
        "SELECT comment_insert(?, ?, ?, ?)",
        [$int_thread_id, $int_user_id, $int_comment_parent_id, $str_text]);
      if($stmt == false){
        $this->set_error(self::QUERY_FAILED);
        return false;
      }
      $result = $stmt->fetchAll();
      if($result == false){
        $this->set_error(self::QUERY_FAILED);
        return false;
      }
      if(is_array($result)
        && is_array($result[0])
          && isset($result[0][0]))
          return $result[0][0];
      return false;
    }
    function query_thread_create(int $author_uuid, int $int_type, int $int_sub_id, string $str_title, string $str_content){
      //base_convert (1000000000, 10, 26);v
      //$_uid = session()->user()->get_user_id();
      //$_uid = 1; //First user in database, aka "Anonymous"
      //type, target, creator, title
      $stmt = $this->execute_prepared("SELECT thread_create(?, ?, ?, ?, ?)", [$int_type, $int_sub_id, $author_uuid, $str_title, $str_content]);
      if($stmt == false){
        $this->set_error(self::QUERY_FAILED);
        return false;
      }
      $result = $stmt->fetchAll(
        //PDO::FETCH_ASSOC
      );
      if($result == false){
        $this->set_error(self::QUERY_FAILED);
        return false;
      }
      if(is_array($result)
        && is_array($result[0])
          && isset($result[0][0]))
          return $result[0][0];
      return false;
    }
    function query_thread_info(int $thread_id) : int{
      $this->_set_result(NULL);
      $stmt = $this->execute_prepared("CALL p_thread_info(?)", [$thread_id]);
      if($stmt == false){
        $this->set_error(self::QUERY_FAILED);
        return false;
      }
      $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
      if($result == false){
        $this->set_error(self::QUERY_FAILED);
        return false;
      }
      if(!is_array($result[0]))
        return false;
      $this->_set_result($result[0]);
      return 1;
    }
    function query_thread_exists(int $thread_id){
      $stmt = $this->execute_prepared("SELECT thread_exists(?)", [$thread_id]);
      if($stmt == false){
        $this->set_error(self::QUERY_FAILED);
        return false;
      }
      $result = $stmt->fetchAll();
      if($result == false){
        $this->set_error(self::QUERY_FAILED);
        return false;
      }
      if(is_array($result)
        && is_array($result[0])
          && isset($result[0][0]))
          return $result[0][0];
      return false;
    }
    function query_thread_list(int $sub_id, int $sorting, int $past_date_stop){
      $cache = new c_memcache_controller();
      $handler = new c_memcache_common_handler();
      $_cache_key = c_cache_db_common_bindings::SUB_THREAD_PAGE_CACHE . $sub_id . $sorting;

      if($handler->object_exists($cache, $_cache_key)){
        $val = $handler->object_get($cache, $_cache_key);
        //if($val)
        //  return $val;
      }
      $stmt = $this->execute_prepared("CALL p_get_subthread_page(?, ?, ?, ?)", [$sub_id , 0, 25, 0]);
      if($stmt == false){
        $this->set_error(self::QUERY_FAILED);
        return false;
      }

      $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
      if($result == false){
        $this->set_error(self::QUERY_FAILED);
        return false;
      }

      if(!is_array($result) || count($result) == 0){
        $this->set_error(self::QUERY_FAILED);
        return false;
      }
      //Unavoidable double encode, cache in db then return array
      $_jen = json_encode($result[0]);
      $handler->set_e($cache, $_cache_key, $result[0], 10);
      //print_r($result[0]);
      return $result;
    }
    function query_thread_id_by_name(string $string_name) : int{

    }
    function query_thread_get_tree(int $thread_id, int $node_start_index, int $depth_limit){
      #base_convert (1000000000, 10, 26);
      //GEYDAMBQGAYDAMBQ
      $cache_key = "_c_sub_thread_tree:" . $node_start_index . "," . $thread_id;
      $cache = new c_memcache_controller();
      $handler = new c_memcache_common_handler();
      if($handler->object_exists($cache, $cache_key)){
        $val = $handler->object_get($cache, $cache_key);
        return $val;
      }
      $stmt = $this->execute_prepared(
          "CALL p_get_tree_exp(?, ?, ?, ?)",
          //"CALL p_get_tree_thread(?, ?, ?, ?)",
          [$thread_id, $node_start_index, $depth_limit, 100]);
      if($stmt == false){
        $this->set_error(self::QUERY_FAILED);
        return false;
      }
      #echo "id: " . $thread_id;
      $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
      if($result == false){
        $this->set_error(self::QUERY_FAILED);
        return false;
      }
      if(!is_array($result) || count($result) == 0)
        return false;

      $json = json_encode($result);
      if($json == NULL)
        $this->set_error(self::QUERY_FAILED);
      //print_r("<pre>" . $result. "</pre>");

      //$handler->set_e($cache, $cache_key, $json, 1);
      return $json;
    }
    //end
  };
  class c_db_service_connection extends c_db{
    function query_status() : int{
      /* returns array with value for each service */
    }
  }
  class c_db_stats_connection extends c_db{
    //Expects an integet between 0 and 0+n
    function query_get_num_active_session() : int{
      $ret = $this->execute_prepared("SELECT stats_get_active_sessions()", [], 1);
      if($ret == NULL || $ret == false || !is_array($ret) || count($ret) != 2) //if anything but an array with two values, return false
        return self::QUERY_UNEXPECTED_RESULT;
      if($ret[0] < 0)
        return self::QUERY_UNEXPECTED_RESULT;
      return $ret[0];
    }
  }
  class c_db_user_connection extends c_db{
    const SESSION_NULL = -5;
    const ACCOUNT_EXISTS = -6;
    function query_user_create(
        string $key,
        string $account_name, string $email, int $optional_age) : int{
      //echo("Creating account...");
      $ret = $this->query_account_exists($account_name, "");
      if($ret < 0)
        return -1;
      else if($ret == 0)
        return false;
      $hash_key = password_hash($key, PASSWORD_DEFAULT);
      $hash_info = password_get_info($hash_key);
      //echo $hash_info['algo'];
      $stmt = self::getInstance()->prepare('SELECT create_account(?, ?, ?, ?)');
      if($stmt == false)
        return self::QUERY_FAILED;
      $ret = $stmt->execute([$account_name, $email, $hash_key, "7.1:PASSWORD_DEFAULT", ]);
      if(!$ret)
        return self::QUERY_FAILED;
      $result = $stmt->fetch();
      if($result < 0)
          return self::QUERY_FAILED;
      return true;
    }
    function query_account_keyhash($account_id){
      $this->_set_result(NULL);
      $ret = $this->execute_prepared('SELECT account_get_hash(?)', [$account_id]);
      //daprint($ret);
      if($ret == false)
        return $ret;
      $ret = $ret->fetchAll();
      if($ret == false || !is_array($ret) || count($ret[0]) != 2) //if anything but an array with two values, return false
        return self::QUERY_UNEXPECTED_RESULT;
      $this->_set_result($ret[0][0]);
      return $ret[0][0];
    }
    function query_user_logout($user, $key){
      $hash_key = password_hash($key, PASSWORD_DEFAULT);
    }
    function query_user_change_key($user, $key){}
    /* Return id of session, 0 on failure */
    public function query_create_session(int $ip, string $useragent){ //string $activity
      if(self::getInstance() == NULL)
        return $this->get_error();
      //requires ip (int), useragent (string), id (integer)
      $stmt = self::getInstance()->prepare('SELECT create_session(?, ?, ?)');
      if($stmt == false)
        return self::QUERY_FAILED;

      #echo "THIS IS THE IP ADDRESS: " . $ip;
      $ret = $stmt->execute([$ip, $useragent, 1]);
      if(!$ret)
        return self::QUERY_FAILED;
      $result = $stmt->fetch();
      if($result == false)
        return self::QUERY_FAILED;
      if(count($result) != 2)
        return self::QUERY_UNEXPECTED_RESULT;
      return $result[0];
    }
    function query_upgrade_session($state){

    }
    /* Update session, return query id or 0 if doesn't exist */
    function query_update_session(
      int $session_uuid,
      string $activity,
      string $action,
      int $flags){
      $this->query_update_session_args($session_uuid, $activity, $action, 'args', $flags);
    }
    function query_update_session_args(
      int $session_uuid,
      string $activity,
      string $action,
      string $arguments,
      int $flags){
      if(self::getInstance() == NULL)
        return false;
      $stmt = self::getInstance()->prepare('SELECT session_update(?, ?, ?, ?)');
      if(!$stmt)
        return self::QUERY_FAILED;
      $ret = $stmt->execute([$session_uuid, $activity, $action, 0x1]);
      if($ret == false)
        return self::QUERY_FAILED;
      $result = $stmt->fetch();
      if($result == false)
        return self::QUERY_FAILED;
      if(count($result) != 2)
        return self::QUERY_UNEXPECTED_RESULT;
      return $result[0];
    }
    //If the session does not exist, query returns 0, if it has not expired, 1, if it has expired 2
    function query_session_timed_out(int $session_uuid) : int{
      $ret = $this->execute_prepared('SELECT session_expired(?)', [$session_uuid]);
      //daprint($ret);
      if($ret == false)
        return $ret;
      $ret = $ret->fetchAll();
      if($ret == false || !is_array($ret) || count($ret[0]) != 2) //if anything but an array with two values, return false
        return self::QUERY_UNEXPECTED_RESULT;
      if($ret[0][0] < 0 || $ret[0][0] > 2) //make sure the result is a valid range
        return self::QUERY_UNEXPECTED_RESULT;
      return (int)$ret[0][0];
    }
    function query_account_exists(string $str_user, string $str_passkey) : int{
      //decho("query_account_exists: " . $user);
      $cache = new c_memcache_controller();
      $handler = new c_memcache_common_handler();
      //$handler->object_get();
      $cache_key = c_cache_db_common_bindings::SUB_CACHE . $str_user;
      if($handler->object_exists($cache, $cache_key)){
        $val = $handler->object_get($cache, $cache_key);
        return $val;
      }
      //c_cache_db_common_bindings::USER_CACHE;
      $ret = $this->execute_prepared('SELECT account_exists(?)', [$str_user], 1);
      if($ret == false)
        return self::QUERY_FAILED;

      $ret = $ret->fetchAll();
      //print_r($ret);
      if($ret == false || !is_array($ret)){ //|| count($ret) != 2){//if anything but an array with two values, return false
        //echo "err";
        return self::QUERY_UNEXPECTED_RESULT;
      }
      //echo $ret[0][0];
      $id = (int)$ret[0][0];
      if($id == 0){
        $handler->set($cache, $cache_key, false);
        return $id;
      }elseif($id > 0){
        $handler->set($cache, $cache_key, $id);
        return $id;
      }else{
        return self::QUERY_UNEXPECTED_RESULT;
      }
      //return (($ret[0] == '0') ? false : (($ret[0] == '1') ? true : false));//self::QUERY_UNEXPECTED_RESULT;

    }
  }
?>
