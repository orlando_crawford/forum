<?php
  class c_enum{
    const NULL = 0x0;
  }
  class enum_userflags extends c_enum{
    const NULL = 0x0;
    const USER_LOGGED_IN = 0x1;
    const USER_SITE_ADMIN = 0x2;
    const USER_ALLOWED_EDITOR = 0x4;
    const USER_ALLOWED_REDIRECTION = 0x8;
  }
  class c_user{
    function __get_ip() : int{
      //$str = "::192.1.1.0";
      /*$str = $_SESSION[c_constant_keys::SESSION_IP];
      if(strlen($str) < 2)
        return 0;
      if(substr($str, 0, 2) == '::') //ipv6 loopback
        $str = substr($str, 2); //clip off ::
      $ret = ip2long($str);*/
    }
    function get_user_id(){
      return $_SESSION[c_constant_keys::USER_ACCOUNT_UUID];
    }
    function is_logged_in() : bool{
      return ($_SESSION[c_constant_keys::SESSION_USER_FLAGS] & enum_userflags::USER_LOGGED_IN) == enum_userflags::USER_LOGGED_IN;
    }
    function get_useragent() : string{
      return $_SERVER['HTTP_USER_AGENT'];
    }
    function userflags(){
      return $_SESSION[c_constant_keys::SESSION_USER_FLAGS];
    }
    function is_admin() : bool{
      return (!($this->userflags() & enum_userflags::USER_LOGGED_IN)
      && $this->userflags() & enum_userflags::USER_SITE_ADMIN);
    }
    function can_access_editor() : bool{
      return is_admin() && $this->userflags() & enum_userflags::USER_ALLOWED_EDITOR;
    }
    function set_logout(){
      $_SESSION[c_constant_keys::SESSION_USER_FLAGS] &= ~enum_userflags::USER_LOGGED_IN;
      $_SESSION[c_constant_keys::USER_ACCOUNT_UUID] = 0;
    }
    function set_login(int $uuid, string $name, string $key) : bool{
      session()->log_action_class($this, __FUNCTION__);
      $_SESSION[c_constant_keys::SESSION_USER_FLAGS] |= enum_userflags::USER_LOGGED_IN;
      //$db = new c_db_user_connection();
      //$ret = $db->query_account_exists('root', 'hash');
      $_SESSION[c_constant_keys::USER_ACCOUNT_UUID] = $uuid;
      return true;
    }
  }
  function user() : c_user{
    return new c_user();
  }
 ?>
