<?php
  abstract class c_controller{
    public function __call(string $name, array $args){
      echo "Failed to run " . $name;
    }
    abstract public function action(string $action);//{ return -1; }
    final public function init(){}
    final public function load(string $action){
      //call switch virtual function function, if overridden by extended class, the action will be switched on
      $this->action($action);
    }
    final public function render(string $index){
        //echo require($index);
    }
  }
?>
