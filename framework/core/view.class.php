<?php
  require_once('/framework/lib/html/html.php');
  define("DEBUG", true, true);


  /* Read view  layout base into class and extract replacement tags, expensive, load this into the cache*/
  class c_view_html_node_table{

  }
class c_view_html_layout{

}
function array_check($a){
  return (isset($a) || array_key_exists($a));
}
function layout_tree_draw($view, $layout){
  echo $layout->get_upper();
  echo $view->render();//render child
  #echo $this->layout()->render();
  //echo $this->get_html_table()->render();
  echo $layout->get_lower();
}
class c_view_layout{
  private $dom = NULL;
  private $selector = NULL;
  private $_tag_table = NULL;
  private $_binding_table = NULL;
  private $_nodes = NULL;
  function _query(){
    $this->_nodes = $this->selector->query("//object[@tagid]");
    foreach($this->_nodes as $_node){
      //echo "<p>Index: " . $_node->getAttribute("tagid") . "</p>";
      $tag_name = $_node->getAttribute("tagid");
      $this->_tag_table[$tag_name] = $_node; //copy, rather than reference value ref will fail to copy some fields
      //array_push($this->_tag_table,
      //  $tag_name => &$_node //["n" => &$_node]
      //  );
    }
  }
  function __construct($layout){
    $this->dom = $layout;
    $this->selector = new DOMXPath($this->dom);
    $this->_view_binding_table = array();
    $this->_tag_table = array();
    $this->_query();
  }
  final public function bind_tag_html(string $tag_region, string $html){
    if(!isset($this->_tag_table[$tag_region]))
      return;
    $replacement = $this->dom->createDocumentFragment();
    $replacement->appendXML($html);

    $node = $this->_tag_table[$tag_region];
    $node->parentNode->replaceChild($replacement, $node);
  }
  final public function get_node(){

  }
  final public function bind_tag_layout(string $tag_region, c_view_layout $region){
    if(!isset($this->_tag_table[$tag_region]))
      return;
    array_push($this->_view_binding_table, array(
          "r" => $tag_region, "v" => &$region
        ));
  }
  final public function bind_tag_view(string $tag_region, c_base_view $region){
    if(!isset($this->_tag_table[$tag_region]))
      return;
    array_push($this->_view_binding_table, array(
          "r" => $tag_region, "v" => &$region
        ));
  }
  function replace_dom_raw(string &$dom, string $tag_target){

  }
  function set($document){

  }
  final public function render() : string{
    #echo "<p>rendering layout....";
    foreach($this->_view_binding_table as $key){
      $text = $key['r'];
      $object = $key['v'];
      #echo "Text: (" . $text . ")";
      $node = $this->_tag_table[$text];
      $replacement = $this->dom->createDocumentFragment();
      $replacement->appendXML($object->render());
      if($node->parentNode)
        $node->parentNode->replaceChild($replacement, $node);
      unset($key);
      //echo "<p>sz: " . count($this->_view_binding_table). "</p>";
    }

    foreach($this->_tag_table as $_tag){
      #print_r($_tag->parentNode);
      $replacement = $this->dom->createDocumentFragment();
      if($_tag && $_tag->parentNode)
        $_tag->parentNode->removeChild($_tag);
    }
    //echo "finish....</p>";
    //print_r($this->dom->saveHTML());
    //$table = $this->selector->query("//object[@tagid]");
    //foreach($table as $_n){
    //  $replacement = $this->dom->createDocumentFragment();
    //  $_n->parentNode->replaceChild($replacement, $_n);
    //}
    return $this->dom->saveHTML($this->dom->documentElement);
  }
  function get_region_tag_matching(string $replace_id){
    //return $this->selector->query("//object[@tagid='" . $replace_id . "']");
  }
}

class c_view_base{

}

class c_view_layout_loader{
  private $doc = NULL;
  private $error = 0;
  function get(){//:c_view_html_node_table{
    return $this->doc; //new c_view_html_node_table($this->doc);
  }
  function ok(){
    return $this->error == false;
  }
  function load($str_html){

  }
  function load_file($str_file){
    #echo "<p>c_view_layout_loader: " . $str_file . "</p>";
    dlog()->line(3, "c_view_layout_loader: " . $str_file);
    $this->error = false;
    $this->doc = new DOMDocument();
    $this->doc->preserveWhiteSpace = false;
    $ret = $this->doc->loadHTMLFile($str_file);
    if($ret == FALSE){
      echo "bad file";
      return $this->error = true;
    }
    return $this->error = false;
  }
}

class c_view_layout_base{
  private $upper = "";
  private $lower = "";
  private $str_title = "";
  private $str_js = "";
  private $str_css = "";
  private $body_table = "";
  final public function set_script(string $str_script){
    $this->str_script = $str_script;
  }
  //final public function set($dom_upper, $dom_lower) { $this->upper; $this->lower; }
  final public function get_lower() : string{
    return "</body><script>" .
      $this->str_script .
      "</script></html>";
  }
  final public function get_upper() : string{
    return "<!DOCTYPE html>
            <html xmlns='http://www.w3.org/1999/xhtml' xmlns='http://www.w3.org/1999/html' lang='en'>" .
              "<meta name='viewport' content='width=device-width, initial-scale=1'>
              <head>" .
                $this->str_css .
                $this->str_js .
                "<title>" .
                  $this->str_title .
                "</title>
                <noscript>
                  Javascript is required for certain parts of the site, please enable it.
                </noscript>

              </head>
              <body>"
              . $this->body_table
              //. "<div class='container'> <div class='content'>text</div></div>"
              ;
  }
  final public function set_css(array $array){
    $idx = count($array);
    if(is_array($array) && $idx > 0){
      for($i = 0; $i < $idx; $i++){
        if( //count($array[$i]) > 0 &&
            isset($array[$i]) && is_string($array[$i]) ){
          $ret = strip_tags($array[$i], "");
          //decho($ret);
          $this->str_css .= "<link rel='stylesheet' type='text/css' href='/asset/css/" . $array[$i] . "' />";
        }
      }
    }
  }
  final public function set_js(array $array){
    $idx = count($array);
    if(is_array($array) && $idx > 0){
      for($i = 0; $i < $idx; $i++){
        if(isset($array[$i]) && is_string($array[$i]) ){
          //echo "loop: " . $i;
          $this->str_js .= "<script src='/asset/js/" . $array[$i] . "' type='text/javascript' /></script>";
        }
      }
    }
  }
  final public function set_title(string $str_title){
      $this->str_title = $str_title;
  }
  final public function set_footer(string $string){

  }
  //final public function set_lower() : string{ return $this->upper;}
}
class c_view_page_renderer extends c_view_base{
  function __construct(){
      decho('c_view_page_renderer');
  }
  final function output(){

  }
}
class c_view extends c_view_base
{
    private $model;
    private $controller;
    private $html;
    public function __construct($controller, $model) {
        $this->controller = controller;
        $this->model = model;
    }
    public function output(){
      $html = html_layout_top();
      $html . html_layout_bottom();
      el('div.container > div.row > div.col-md-6',
        el('a', ['href' => '#'], 'Hello world!')
      );
      //this->render();
    }
};
class c_view_object extends c_view_base{

}
class c_view_object_js extends c_view_base{

};
