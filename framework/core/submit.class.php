<?php
require_once("/session.php");
class enum_post_response{
  const SUCCEEDED = '0';
  const BAD = '-1';
  const NO_SESSION = '-1';
}
class c_post_reciver{
  function _return_response($code){
    exit($code);
  }
  private $array_post = NULL;
  function __construct($object){
    if(!session()->exists()){
      echo "no session";
      return $this->_return_response(enum_post_response::NO_SESSION);
    }
    session()->log_action_class($this, __FUNCTION__);
    if(!isset($array_post) || count($object) == 0){
      $array_post = NULL;
      $this->_return_response(enum_post_response::BAD);
      return;
    }
    echo print_r($_POST);
    $array_post = $object;
  }
  final function get($tag){
    return $array_post[$tag];
  }
  final function get_id() : int{
    return $array_post["id"];
  }
  final function contains($tag){
    if(!isset($array_post[$tag]))
      return false;
    return true;
  }
  function decode($post){
    $array_post = $post;
  }
}
 ?>
