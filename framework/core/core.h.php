<?php

class enum_core_error{
  const ERROR_OK = 0;
  const ERROR_UNEXPECTED = -1;
  const ERROR_DATABASE_CONNECTION_FAILURE = -2;
  const ERROR_SERVICE_UNAVALIABLE = -3;
  const ERROR_SESSION_FAILED_TO_INIT = -4;
  const ERROR_BAD_QUERY = -5;
  const ERROR_PAGE_PARSE_FAULT = -6;
};

/*
class enum_core_error{
  const ERROR_OK = 0;
  const ERROR_UNEXPECTED = -1;
  const ERROR_DATABASE_CONNECTION_FAILURE = -2;
  const ERROR_SERVICE_UNAVALIABLE = -3;
  const ERROR_SESSION_FAILED_TO_INIT = -4;
  const ERROR_PAGE_PARSE_FAULT = -5;
};*/
 ?>
