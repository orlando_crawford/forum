<?php
class c_cache_html{
  function exists(){}
}

class c_memcache_controller{
  static $memcache = NULL;
  static $error = false;
  private function init(){
    //self::$memcache = new Memcache();
    //self::$memcache->addServer("127.0.0.1");
  }
  private function _new(){
    return self::$memcache = new Memcache;
  }
  private function _connect(){
    //can't connect, set error bit
    if(!self::$memcache->connect("127.0.0.1")){
      self::$error = true;
      //echo "c_memcache_controller:: failure";
      return self::$memcache;
    }
    //echo "c_memcache_controller:: connected ok";
    self::$error = false;
    return self::$memcache;
  }
  public function connect($addr){
    _connect($addr);
  }
  private function connected(){
    return !self::$error && !self::$memcache;
  }
  function ok(){
    return !self::$error;
  }
  function bad(){
    return self::$error;
  }
  function get_instance(){
    if(self::$memcache){
      self::$error = false;
      return self::$memcache;
    }
    $this->_new();
    return $this->_connect();
  }
}
class c_memcache_user_cache{
  function user_exists(){}

}
class c_memcache_common_handler {
  private $_get_cache = NULL;
  private $_get_cache_key = NULL;
  //Returns TRUE on success or FALSE on failure. Use Memcached::getResultCode() if necessary.

  final function set_e(
    c_memcache_controller $db,
    string $key,
    $object, int $expiry){
        return $db->get_instance()->set($key, $object, 0, $expiry);
    }
  final function set(
    c_memcache_controller $db,
    string $key, $object) : bool{
    $ret = NULL;
    //if($db->ok())//returns true false on failure
      return $db->get_instance()->set($key, $object, 0, 0);
    //return false;
  }
  //returns false or array
  final function object_get(c_memcache_controller $db, string $querykey) {
    $timer = new c_timer();
    $timer->start();

    if($this->_get_cache != NULL && ($this->_get_cache_key == $querykey)){
      dlog()->line(3, "Object exists in quick cache... using result: " . $querykey . " Hit: 0ms");
      return $this->_get_cache;
    }
    dlog()->line(3, "Object missed in quick cache... fetching from db" . $querykey);
    $ret = $db->get_instance()->get($querykey);
    $timer->stop();
    dlog()->line(3, "Cache speed: " . $timer->get_elapsed_ms() . "ms");
    if($ret == FALSE)
      return false;
    else
      return $ret;
  }
  //Local cache to check existance, returns false if failed to get, or failed to hit server
  final function object_exists(c_memcache_controller $db, string $querykey) : bool{
    $ret = false;
    //if($db->ok())
    $ret = $db->get_instance()->get($querykey);
    //$db->get_instance()->getResultCode();
    dlog()->line(3, "memcache hit:" . (($ret == FALSE) ? "false" : "true") . $querykey);

    //Cache result
    //get value can be mixed, or false, return false if $ret == FALSE
    if($ret == FALSE){
      $this->_get_cache = NULL;
      $this->_get_cache_key = NULL;
      return false;
    }
    $this->_get_cache = $ret;
    $this->_get_cache_key = $querykey;
    return true;
    //else return true;
  }
}
 ?>
