<?php
  require_once('/user.class.php');
  require_once(__DIR__ . '\../database/database.class.php');
  require_once('/router.class.php');
  require_once('/core.h.php');
  require_once('/log.php');
  require_once('/cache.class.php');
  class enum_const_svars{
    const SESSION_TIMEOUT = 1800;
    const SESSION_DEBUG = true;
  }
  class c_constant_keys{
    const SESSION_ACTIVITY_TIMEOUT = 'u_t'; /* Session timeout */
    const SESSION_USER_FLAGS = 'u_f';     /* User flags */
    const SESSION_USER_USERNAME = 'u_u';  /* Username, if logged in */
    const SESSION_IP = 'u_ip';
    const SESSION_IP_UNPACKED = 'u_ip_u'; /* String ip */
    const SESSION_FLAG = 'sf'; /* Session already exists in session variable */
    const SESSION_USER_AGENT = 'u_a';
    const SESSION_ID = 'u_id'; /* Session id allocated by session database */
    const USER_ACCOUNT_UUID = 'u_ac_uuid'; /* User account uuid, > 0 if logged in, actions are attributed to this uuid */
    const USER_INTENT = 'u_ac_intent';
    const SESSION_LENGTH_TIMESTAMP = 'u_tlen';
    const GLOBAL_SERVICE_STATS = 'g_stats';
    const REQUEST_INTENT = 'r_i';
    const REQUEST_INTENT_PREVIOUS = 'r_i_p';
  };
  class c_intent{
    function get_action(){

    }
    function get_sub(){

    }
    function get_thread(){

    }
    function get_comment(){

    }
  }
  function get_client_ip_str() : string{
    $ipaddress = '';
    $ipaddress = getenv ( "REMOTE_ADDR" );
    /* Todo: flick through this in XCODE */
    //the following is horribly insecure

    /*if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';*/
    return $ipaddress;
  }
  function ipaddress_to_ipnumber($ipaddress) {
	   $pton = @inet_pton($ipaddress);
	    if (!$pton) { return false; }
      $number = '';
      foreach (unpack('C*', $pton) as $byte) {
        $number .= str_pad(decbin($byte), 8, '0', STR_PAD_LEFT);
    }
    return base_convert(ltrim($number, '0'), 2, 10);
  }
  function session_time_duration(): int{
    return (time() - $_SESSION[c_constant_keys::SESSION_LENGTH_TIMESTAMP]);
  }
  function is_session_expired() : bool{
    echo 'is_session_expired';
    if (isset($_SESSION[c_constant_keys::SESSION_ACTIVITY_TIMEOUT])
      && (time() - $_SESSION[c_constant_keys::SESSION_ACTIVITY_TIMEOUT] > enum_const_svars::SESSION_TIMEOUT)) {
      // last request was more than 30 minutes ago
      session_unset();     // unset $_SESSION variable for the run-time
      session_destroy();   // destroy session data in storage
      return true;
    }
    $_SESSION[c_constant_keys::SESSION_ACTIVITY_TIMEOUT] = time(); // update last activity time stamp
    return false;
  }
  function core_session_exists() : bool{
    return (session_status() == PHP_SESSION_ACTIVE);
  }
  class c_session{
    const SESSION_FAILED_TO_INIT = -1;
    const SESSION_NEW = 0;
    const SESSION_RESUMED = 1;
    const SESSION_WILL_REGENERATE = 2;
    private static $_timer = NULL;
    function __construct(){
      if(!self::$_timer){
        self::$_timer = new c_timer();
        self::$_timer->start();
      }
    }
    function timer() : c_timer{
      return self::$_timer;
    }
    function has($key) : boolean {
      return $_SESSION[$key];
    }
    function http_referer() : string{
      if(!isset($_SERVER['HTTP_REFERER']))
        return '0';
      return $_SERVER['HTTP_REFERER'];
    }
    function get_id() : int{
      return $_SESSION[c_constant_keys::SESSION_ID];
    }
    function client_address() : int{
      return $_SESSION[c_constant_keys::SESSION_IP_UNPACKED];
    }
    function client_address_str() : string{
      return $_SESSION[c_constant_keys::SESSION_IP];
    }
    function last_access() : integer {
      return $_SESSION[c_constant_keys::SESSION_ACTIVITY_TIMEOUT];
    }
    function user(){
      return new c_user();
    }
    //$_SERVER['REQUEST_URI']
    function session_expired(){
      return is_session_expired();
    }
    function log_action_class($caller_class, string $function_name) : bool{
      $db = new c_db_user_connection();
      $class_name =  get_class($caller_class);
      $class_name = is_string($class_name) ? $class_name : "unknown_class";
      $function_name = is_string($function_name) ? $function_name : "unknown_func";
      $ret = $db->query_update_session($this->get_id(),  $class_name, $function_name, 1);
      return true;
    }
    function destroy(){
      echo 'destroy';
      session_unset($_SESSION);   // remove all session variables
      session_destroy();          // destroy the session
    }
    function timeout_get(){
      //Remove function?, can't see a reason that we'd need to know the real timeout value
      //$_SESSION[c_constant_keys::SESSION_ACTIVITY_TIMEOUT];
    }
    function timeout_update(){
      $_SESSION[c_constant_keys::SESSION_ACTIVITY_TIMEOUT] = time();
    }
    function print_session(){
      $m = floor(session_time_duration() / 60);
      $s = session_time_duration();
      printf("Session:<br>\tTime:%d(s)<br>\tTime:%d:%d(m)<br>\tTimeout%d<br>Session id:%d<br>\tAccount id: %d<br>Flags: %s<br>\tIp:%d",
        $s,
        $m,
        session_time_duration() - (60 * $m),
        $_SESSION[c_constant_keys::SESSION_ACTIVITY_TIMEOUT],
        $_SESSION[c_constant_keys::SESSION_ID],
        $_SESSION[c_constant_keys::USER_ACCOUNT_UUID],
        "Flags: " . (
          ($_SESSION[c_constant_keys::SESSION_USER_FLAGS] & enum_userflags::USER_SITE_ADMIN)
            == enum_userflags::USER_SITE_ADMIN) ? "Is admin" : "",
        $_SESSION[c_constant_keys::SESSION_IP_UNPACKED]);
    }
    function cookie_check(){
      return false;
      session_start();
      $a = session_id();
      session_destroy();

      session_start();
      $b = session_id();
      session_destroy();
      if ($a == $b)
        echo "<p>Cookies ON</p>";
      else
        echo "<p>Cookies OFF</p>";
    }
    function exists() : bool{
      return init() && isset($_SESSION[c_constant_keys::SESSION_FLAG]);
    }
    function init() : int{
      if (!is_writable(session_save_path()))
          dlog()->line(3, 'Session path "'.session_save_path().'" is not writable for PHP!');
      else {
        dlog()->line(3, "Session table ok");
      }
      session_start();

      $cache = new c_memcache_controller();
      $cache->get_instance();
      $cache->ok();
      //$this->destroy();
      dlog()->line(3, "[Browser] " . (isBrowserMobile() ? "Mobile browser" : "Desktop browser"));
      if(isset(
          $_SESSION[c_constant_keys::SESSION_FLAG]) &&
        $this->get_id() > 0){ //if
        dlog()->line(3, '[Session] exists');
        //session_regenerate_id(false);

        //echo "client_ip: " . get_client_ip_server();
        //echo "stored ip: " . $this->address();
        //echo "convert: " . ipaddress_to_ipnumber(get_client_ip_server());

        /* If the current ip doesn't match the address stored when the session was created, then destroy it */
        if($this->client_address_str() != get_client_ip_str()){
          $this->destroy();
          router()->navigate_root(); //move to index and force refresh
          exit(0); /* nuke the session */
        }

        $db = new c_db_user_connection();
        $b_exists = $db->query_session_timed_out($this->get_id());
        if($b_exists < 0)
          return enum_core_error::ERROR_DATABASE_CONNECTION_FAILURE;
        if($b_exists == 2){//database considers session expired
          dlog()->line(3, '[Session] Session expired');
          $ret = $db->query_update_session($this->get_id(), 'session', 'expired,closed', 1);
          if($ret < 0)
            return enum_core_error::ERROR_DATABASE_CONNECTION_FAILURE;
          //dlog()->line(3, '[Session] Regenerating new, redirecting to home');
          //echo "SESSION ENDED";
          //router()->navigate_root(); //move to index and force refresh
          $this->destroy(); //destroy current session
          //exit(0); //terminate execution, if the browser ignores and doesn't reconnect, they'll still regenerate if the user manually refreshes (F5)
          return $this->init();
        }
        $ret = $db->query_update_session($this->get_id(), 'session', 'resume', 1);
        if($ret < 0)
          return enum_core_error::ERROR_DATABASE_CONNECTION_FAILURE;
        dlog()->line(3, '[Session] Database id ' . $this->get_id());
        return false;
      }
      if(!isset($_SESSION[c_constant_keys::SESSION_IP])
        || !isset($_SESSION['ua'])){}
      dlog()->line(3, '[Session] Creating new session');
      $_SESSION[c_constant_keys::GLOBAL_SERVICE_STATS] = array(
        "anon_online" => 0,
        "acc_online" => 0
      );

      $_SESSION[c_constant_keys::SESSION_FLAG] = true;
      $_SESSION[c_constant_keys::SESSION_ID] = 0; //if
      $_SESSION[c_constant_keys::SESSION_LENGTH_TIMESTAMP] = time();

      $str_ip = get_client_ip_str();

      if(filter_var($str_ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
        echo "Valid IPv4";
      }
      else if(filter_var($str_ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)) {
          echo "Valid IPv6";
      }else{
        echo "Invalid Address";
        exit(0);
      }

      $_SESSION[c_constant_keys::SESSION_IP] = $str_ip;
      $_SESSION[c_constant_keys::SESSION_IP_UNPACKED] = ipaddress_to_ipnumber($str_ip);

      $_SESSION[c_constant_keys::USER_ACCOUNT_UUID] = 0;
      //$_SESSION[c_constant_keys::SESSION_USER_AGENT] = $_SERVER['HTTP_USER_AGENT'];
      $_SESSION[c_constant_keys::SESSION_USER_FLAGS] = enum_userflags::NULL;
      $_SESSION[c_constant_keys::SESSION_USER_USERNAME] = '';
      $_SESSION[c_constant_keys::SESSION_ACTIVITY_TIMEOUT] = time();

      $db = new c_db_user_connection();
      $id = $db->query_create_session(
          session()->client_address(),
          session()->user()->get_useragent());

      dlog()->line(3, $id);
      if($id < 0){
        dlog()->line(3, "[session] failed to open new session, database could not be reached");
        $this->destroy();
        return c_session::SESSION_FAILED_TO_INIT;
      }
      $_SESSION[c_constant_keys::SESSION_ID] = $id;
      $ret = $db->query_update_session(
        $_SESSION[c_constant_keys::SESSION_ID],
        'session', 'start',
        0x1);
      if($ret < 0){
        router()->navigate_root();
        return enum_core_error::ERROR_DATABASE_CONNECTION_FAILURE;
      }
      dlog()->line(3, '[Session] IP ' . session()->client_address_str());
      dlog()->line(3, '[Session] Database session id ' . $id);
      return self::SESSION_NEW;
    }
  }
  function session(){
    return new c_session();
  }
 ?>
