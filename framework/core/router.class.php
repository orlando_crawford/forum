<?php
  require_once(__DIR__ . '\../database/database.class.php');
  require_once(__DIR__ . '\../lib/regex.php');
  require_once('/core.h.php');
  require_once(__DIR__ . '\../../application/controllers/admin.controller.php');
  require_once(__DIR__ . '\../../application/controllers/basepage.controller.php');
  require_once(__DIR__ . '\../../application/controllers/api.controller.php');
  require_once(__DIR__ . '\../../application/controllers/sub.controller.php');
  class c_router{
    final function _load($file){
      echo $file;
      if (!is_readable($file) || is_dir($file))
        return false;
      require($file);
      //file_get_contents($file);
      return true;
    }
    function navigate_error($int_type){
      switch($int_type){
        case 1:
          header("Refresh:0; url='/error.php'");
          break;
      }
      exit(1);
    }
    final function navigate_root(){
      session()->log_action_class($this, __FUNCTION__);
      dlog()->line(3, 'navigate_root');
      header("Refresh:0; url='/'");
      exit(1);
    }
    final function navigate(string $site, string $action, string $args) : int{
      session()->log_action_class($this, __FUNCTION__);
      //if($id < 0) return -1;
      //$ret = regex_file_ending($args);
      //$id_page = $db->query_page_exists($id);
      //if($id_page < 0)
      //  return enum_core_error::ERROR_DATABASE_CONNECTION_FAILURE;*/
      //decho(count($ret) > 0 ? 'URI requesting file (.*)' : 'URL requesting page');
      //session()->user()->is_admin();
      libxml_use_internal_errors(true);

      dlog()->line(3, "Router: +++++");
      dlog()->line(3, $site);
      dlog()->line(3, $action);
      switch($site){
        case "home":
          break;
        //case "submit":break;
        case "api":
          $api = new c_controller_api();
          if($api->load($action) < 0);
            return 0;
          break;
        case "user":
          $control = new c_sub_controller();
          if($control->action($action, $args) < 0)
            return $this->navigate_root();
          break;
        case "sub":
          //array_slice($_GET['intent']);
          //session()->print_session();
          #echo "Checking if board exists: " . $action;
          #echo "subq: " . $args;
          //if($id == 0)
          //  return 0;//enum_core_error::ERROR_BAD_QUERY;
          #echo "Sub: " . $id;
          //dlog()->line(3, $id);
          $control = new c_sub_controller();
          if($control->action($action, $args) < 0)
            return $this->navigate_root();
          break;
        case "admin":

          break;
        case "index":
          //session()->print_session();
          //$this->_load("/application/controllers/basepage.controller.php");
          $control = new c_basepage_controller();
          if($control->load($action) < 0)
            return 0;
        break;
        default:
          echo "no router path";
          echo $site;
          exit(0);
          //log()->log_printf(0, "Type: %d", "%id");
          return enum_core_error::ERROR_BAD_QUERY;
          break;
      }
      libxml_clear_errors();
      dlog()->line(3, "+++++");
      return enum_core_error::ERROR_OK;
    }
  }
  class c_app_router extends c_router{

  }
  function router(){
    return new c_app_router();
  }

 ?>
