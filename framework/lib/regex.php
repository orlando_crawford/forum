<?php
function regex_file_ending($string) : Array{ /*validate string to have a .* ending*/
  //.*\.([A-z]+) //somefile.ending
  preg_match('/.*\.([A-z]+)/', $string, $matches, PREG_OFFSET_CAPTURE);
  //print_r($matches);
  return $matches;
}
 ?>
