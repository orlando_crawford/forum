<?php
declare(strict_types=1);

function url_is_embeddable(string $str_url) : int{
  $url = "http://www.google.com/";
  $url_headers = get_headers($url);
  foreach ($url_headers as $key => $value)
  {
    $x_frame_options_deny = strpos(strtolower($url_headers[$key]), strtolower('X-Frame-Options: DENY'));
    $x_frame_options_sameorigin = strpos(strtolower($url_headers[$key]), strtolower('X-Frame-Options: SAMEORIGIN'));
    $x_frame_options_allow_from = strpos(strtolower($url_headers[$key]), strtolower('X-Frame-Options: ALLOW-FROM'));
    if ($x_frame_options_deny !== false || $x_frame_options_sameorigin !== false || $x_frame_options_allow_from !== false)
    {
        return false;
    }
  }
  return true;
}
function get_id_from_string(string $in) : int{
  $ret = base_convert($in, 32, 10);
  if($ret == "0" && $in != '0')
    return 0;
  return (int)$ret;
}
function get_string_id(int $in){
  return base_convert($in, 10, 32);
}

function xssafe($data, $encoding='UTF-8')
{
   return htmlspecialchars($data,ENT_QUOTES | ENT_HTML401,$encoding);
}
function xecho($data)
{
   echo xssafe($data);
}

/*
  Note: Redundant, though it's an example
function isInt($type) : bool {
  return gettype($type) == 'integer';
}
function isString($type) : bool{
  return gettype($type) == 'string';
}*/
function func(){

}
function isBrowserMobile(){
  return;
  /*
  $useragent=$_SERVER['HTTP_USER_AGENT'];
  if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)))
func());*/

//header('Location: http://detectmobilebrowser.com/mobile');
}
function _dprint($string){
  echo print($string);
}
function _dtprintf($string, $string_tag){
  echo '<' . $string_tag . '>' . printf($string) . '</' . $string_tag . '>';
}
function _dprintf($string){
  echo '<p>' . printf($string) . '</p>';
}
function _decho($string){
  echo '<p>[' . $string . ']</p>';
}
function _daprint($array){
  echo '<pre>'; print_r($array); echo '</pre>';
}
function isSecure() : bool{
  return
    (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off')
    || $_SERVER['SERVER_PORT'] == 443;
}
function getAddress() : string{
  $protocol = ((isSecure()) && ($_SERVER['HTTPS'] == 'on') ? 'https' : 'http');
  return $protocol.'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
}
function check_session(){
  echo "Checking session...";
  if(!$has_session)
    start_session();

  is_session_expired();
  session_start();
}
function has_session() : bool{
  if(!isset($_SESSION['LAST_ACTIVITY']))
      return false; //$has_session =
  return session_status() == PHP_SESSION_ACTIVE;
}
function get_xml_parser(){
  return $xml = simplexml_load_string($myXMLData);// or die("Error: Cannot create object");
}
function get_ip(){
  return getenv ( "REMOTE_ADDR" );
}
function ABORT_ERROR_CODE(int $errorcode) : string{
  $table_generic_error = Array(
      "UNKNOWN_ERROR_CODE",
      "NO_ERROR",
      "DATABASE_UNAVAILABLE",
      "SERVICE_UNAVAILABLE",
      "NO_QUERY"
    );
  if(is_integer($errorcode) && $errorcode > 0 && $errorcode < 6)
    return $table_generic_error[$errorcode] . ' ' . $errorcode;
  else //unknown error code
    return $table_generic_error[0]  . ' ' . $errorcode;
}
function ABORT_ERROR($string, $errorcode){
  echo '<h1>' . $string . '</h1><h2>Error code: ' . ABORT_ERROR_CODE($errorcode) . '</h2>';
  exit($errorcode);
}
