<?php
function el(string $tag, $attributes = null, $content = null) : string
{
    return \HtmlElement\HtmlElement::render(...func_get_args());
}
 ?>
