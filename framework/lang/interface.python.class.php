<?php
  abstract class c_interface{
    private $_ret;
    private $_code;
    final protected function _set_result(int $code, string $var){
      $this->_ret = $var;
      $this->_code = $code;
    }
    final function result(){
      return $this->_ret;
    }
    abstract function execute(string $proc, string $args) : int;
    //{
      //exec($proc, $this->_ret);
      //count($this->_ret);
      //return $this->_ret;
    //}
  }
  class c_interface_python extends c_interface{
    public function execute(string $proc, string $arg_string): int{
      $dir = dirname(__FILE__);
      //var_dump($dir);
      $path = $dir . DIRECTORY_SEPARATOR . 'python3\python-markup.py';
      //'C:\Users\Orlando\AppData\Local\Programs\Python\Python36-32\python.exe ' . $dir . DIRECTORY_SEPARATOR . 'python-markup.py';
      echo "path: " . $path;
      $input = $arg_string;
      $descriptorspec = array(
        0 => array("pipe", "r"),
        1 => array("pipe", "w"),
        2 => array("file", "error-output.txt", "a"));
      $process = proc_open(
          'G:\bin\Python\Python37-32\python.exe ' . $path . ' ' . $input,//'python.exe lol',
          $descriptorspec,
          $pipes);
      if($process == FALSE || !is_resource($process)) {
        echo "[!failed!]";
      }
      echo gettype($process);
      /*while( ! feof($pipes[1])){
        $return_message = fgets($pipes[1], 1024);
        if (strlen($return_message) == 0) break;
        echo $return_message.'<br />';
        ob_flush();
        flush();
      }*/

      fwrite($pipes[0], $input);
      fclose($pipes[0]);

      stream_set_timeout($pipes[1], 10);
      $ret = stream_get_contents($pipes[1]);
      if($ret == FALSE)
        echo "failed to read stream";

      echo "<p>result: " . $ret . "</p>";
      fclose($pipes[1]);
      // It is important that you close any pipes before calling
      // proc_close in order to avoid a deadlock
      $return_value = proc_close($process);
      echo "command returned: " . $return_value;
      return 1;
    }
  }
 ?>
