from datetime import datetime as dt
from datetime import timedelta

from math import log
import time
import calendar

epoch = dt(1970, 1, 1)
###########
#
#   Inspiration taken from the old reddit opensource
#   Because I'm fairly thick, and this seems like a clever idea

def sqltime_to_unix(in_timestamp):
    print("SQL Timestamp:", in_timestamp)
    mysql_time_struct = time.strptime(in_timestamp, '%Y-%m-%d %H:%M:%S')
    mysql_time_epoch = calendar.timegm(mysql_time_struct)
    return mysql_time_epoch

def epoch_seconds(date):
    td = (dt.fromtimestamp(date) - epoch)
    return td.days * 86400 + td.seconds + (float(td.microseconds) / 1000000)

def score(ups, downs):
    return ups - downs

def hot(ups, downs, date):
    print("hot -> ups: ", ups, "downs: ", downs, "date: ", date)
    s = score(ups, downs)
    order = log(max(abs(s), 1), 10)
    sign = 1 if s > 0 else -1 if s < 0 else 0
    seconds = (epoch_seconds(date) - 1134028003) #seconds since unix epoch

    print("Seconds: ", seconds)
    return round((sign * order) + (seconds / 45000), 7)
#SELECT int_voteup, int_votedown, int_hotness FROM thread WHERE date_lastpost > DATE_SUB(NOW(), INTERVAL 1 DAY);
#fetch compute and update recently updated threads to calculate 'hotness'
