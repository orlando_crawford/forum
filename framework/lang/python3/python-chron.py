import mysql.connector
import time
import python_score
import datetime

def print_unix_time(_timestamp):
    print("Timestamp: ", datetime.datetime.fromtimestamp(int(_timestamp)).strftime('%Y-%m-%d %H:%M:%S'))
def thread_score():
    _start = time.time();
    print("python-score -> started");
    mydb = mysql.connector.connect(
        host="localhost",
        port="3305",
        user="root",
        passwd="password",
        database="forum")
    mycursor = mydb.cursor()
    mycursor.execute(
        """SELECT
            id,
            int_voteup,
            int_votedown,
            float_hotness,
            date_lastpost
        FROM thread
        WHERE is_archived <> 1
         AND date_lastpost > DATE_SUB(NOW(), INTERVAL 1 DAY);""")
    myresult = mycursor.fetchall()
    _len = len(myresult)
    print("-> num rows ", _len)
    for row in myresult:
        print(str(row[4]))
        dstr = str(row[4]);
        _time = python_score.sqltime_to_unix(dstr)
        print(_time)
        print_unix_time(_time);
        #_diff = (time.time() - _time)
        #print(type(_time), int(_diff))
        #print(.strftime('%Y-%m-%d %H:%M:%S'))
        _ago = (datetime.datetime.now() - datetime.datetime.fromtimestamp(round(_time / 1000)));
        print(_ago);
        score = python_score.hot(row[1], row[2], _time)
        _iscore = score #int(score);
        #print("-> ", row[0], row[1])
        print("thread [id=", row[0], ",score=", score, "] ", end="")
        mycursor.execute("UPDATE thread SET float_hotness = " + str(_iscore) + " WHERE id = " + str(row[0]) + ";")
        print("updated")
    print("-> commiting... ")
    mydb.commit()
    print("-> ok");
    _end = time.time();
    print("python-score -> end {:f} ms" .format((_end - _start) * 1000 ))

def main():
    #chron job
    #subprocess.call(["php", "/path/to/your/script.php"])
    thread_score()

if __name__ == '__main__':
    main()
