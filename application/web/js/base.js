var session;
function __init__(){
  /* bootstrap session information and global objects */
  console.log("__init__");
  let _logged_in = getCookie('ctest');
  if(!_logged_in)
    setCookie('ctest', '0', 1);
  if(!getCookie('ctest')){
    console.log("Failed to write cookie");
    alert("Failed to write cookie.\n:Your cookies appear to be disabled.\nUnfortunately cookies are required to make this website work correctly.\nPlease enable them for this website\nDon't worry, we're not selling your information to china, we need to use them to store a few things as you browse around.")
    return;
  }
  session = c_user_session();
  session.load();
  //let s = (c_user_session()).load();
  f_init();

  $('#myModal').on('shown.bs.modal', function () {
    $('#myInput').trigger('focus')
})
}
