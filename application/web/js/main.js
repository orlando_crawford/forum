var glob = {
  id: -1,
  where: 0,
  name: "",
}

var cb_ajax_success_tree = function(data, textStatus, jqXHR){
  if(data.length > 1 && data[0] == '-' && data[1] == '1') return;
    //matches '-1', if it returns correctly, it will return json, (json array starting with '[{' )
  f_draw_thread_tree(data);
};//.bind(this);

var g_query_history;
function push_query_history(In_ob){
  g_query_history = In_ob;
}
function f_set_attrib(ob, attrib, name){

}
function conv_sqlDate(In_dateStr){
  //returned from mysql timestamp/datetime field
  let a = In_dateStr.split(" ");
  let d = a[0].split("-");
  let t = a[1].split(":");
  return new Date(d[0],(d[1]-1),d[2],t[0],t[1],t[2]);
}
function f_generate_thread_row(
  In_str_id,
  In_str_title,
  In_str_author_name,
  In_str_date_sql
){
  let date = conv_sqlDate(In_str_date_sql);
  let div = document.createElement("div");
  div.setAttribute("a_id", In_str_author_id);
  div.setAttribute("class", "comment");

}
function f_node_set_tooltip(In_node, str_text){
  In_node.setAttribute("title", str_text);
  return In_node;
}
function f_daydiff(In_str_date){
  var date1 = new Date(In_str_date);
  var date2 = Date.now();
  var timeDiff = Math.abs(date2 - date1.getTime());
  var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
  return diffDays;
}
function f_generate_comment(
    In_str_author_id,
    In_str_author_name
  ){
  let div = document.createElement("div");
  div.setAttribute("a_id", In_str_author_id);
  div.setAttribute("class", "comment");
  return div;
}
function f_node_append_text(In_node, str_text){
  let text = document.createTextNode(str_text);
  In_node.appendChild(text);
  return In_node;
}
const USER_FLAGS_ADMIN = 0x8;
const USER_FLAGS_MODERATOR = 0x4;
function f_generate_thread_link2(In_post){
  let root = document.createElement("div");
  root.setAttribute("class", "post-container media p-sm-1 m-sm-1 p-md-1 m-md-1 link square");

  let inner = document.createElement("div");
  inner.setAttribute("a-id", In_post['uint_aid']);
  inner.setAttribute("post-id", In_post['str_id']);
  inner.setAttribute("class", "media-body");

  let title = document.createElement("h6");
  title.setAttribute("class", "mt-0 hoverable clickable action");
  title.setAttribute("binding-actiontype", "button");
  title.setAttribute("binding-action-id", "threadlink");

  //f_node_append_text(inner, In_post["str_title"]);
  f_node_append_text(title, In_post["str_title"]);
  let infobar = document.createElement("div");
  infobar.setAttribute("class", "infobar");
  //infobar.appendChild(fast_pill(In_post['id']));

  let node_usertext = document.createElement("span");
  f_node_set_tooltip(node_usertext,
      "" + ((0 == 1) ? "[administrator]" : "[user]"));
  node_usertext.setAttribute("class", "hoverable user " + ((0 == 1) ? "user-admin" : ""));
  node_usertext.setAttribute("onclick", "f_context_onclick(this)");
  node_usertext.setAttribute("onmouseover", "f_context_onhover(this)");
  infobar.appendChild(f_node_append_text(node_usertext, '+' + In_post['str_aname']));

  let img = document.createElement("img");
  img.setAttribute("class", "align-self-center mr-3 small");
  img.setAttribute("src", "/asset/media/img/placeholder64_2.png");
  img.setAttribute("alt", "Generic");
  root.appendChild(img);

  inner.appendChild(infobar);
  inner.append(title);
  root.appendChild(inner);
  return root;
}
function f_generate_thread_link(
    In_post
  ){
  if(In_post["str_aname"] == null)
    return -1;
  //let _flags =
  let _flags = In_post["baflags"];// |= (0x8|0x10);
  let root = document.createElement("div");
  root.setAttribute("class", "post-container");

  let inner = document.createElement("div");
  inner.setAttribute("class", "post link-thread _highlight-underline");
  inner.setAttribute("a-id", In_post['uint_aid']);
  inner.setAttribute("post-id", In_post['str_id']);

  let message = document.createElement("div");
  message.setAttribute("class", "post-message clickable post-title");
  let str_text = In_post["str_title"];
  //const POST_THREAD_TITLE_STR_CLIP = 128;
  //if(str_text.length > POST_THREAD_TITLE_STR_CLIP)
  //  str_text = str_text.substring(0, POST_THREAD_TITLE_STR_CLIP) + "...";
  let title = document.createTextNode(str_text);
  message.appendChild(title);
  inner.appendChild(message);

  let infobar = document.createElement("div");
  infobar.setAttribute("class", "infobar");

  let realid = parseInt(In_post['str_id'], 32);
  let node_postinfo = document.createElement("span");
  f_node_set_tooltip(node_postinfo, "[Post id]:, #" + realid);
  node_postinfo.setAttribute("class", "hoverable");
  node_postinfo.setAttribute("onclick", "f_context_onclick(this)");
  infobar.appendChild(f_node_append_text(node_postinfo, '#' + In_post['str_id']));

  let _level = ((_flags & 0x1C) >> 2);
  let _ismod = 0;
  let node_usertext = document.createElement("span");
  f_node_set_tooltip(node_usertext,
      "" + ((_level == 1) ? "[administrator]" : "[user]"));
  node_usertext.setAttribute("class", "hoverable user " + ((_level == 1) ? "user-admin" : ""));
  node_usertext.setAttribute("onclick", "f_context_onclick(this)");
  node_usertext.setAttribute("onmouseover", "f_context_onhover(this)");
  infobar.appendChild(f_node_append_text(node_usertext, '+' + In_post['str_aname']));


  let node_postnum = document.createElement("span");
  node_postnum.setAttribute("class", "hoverable user ");
  f_node_append_text(node_postnum, In_post['uint_pcnt']);
  f_node_set_tooltip(node_postnum, "Post count" + ((In_post['uint_pcnt']) == 0 ? "" : "") );
  infobar.appendChild(node_postnum);

  let diffDays = f_daydiff(In_post["date_create"]);
  let node_date = document.createElement("span");
  f_node_set_tooltip(node_date,
      In_post["date_create"]);
  infobar.appendChild(f_node_append_text(node_date, (diffDays < 7)
    ? diffDays + " day" + ((diffDays > 1) ? "s" : "") + " ago.": (diffDays / 7) + " weeks ago" ));

  inner.appendChild(infobar);
  root.appendChild(inner);
  return root;
}
function f_context_onclick(e){
  let elem = e.parentNode.parentNode.parentNode;
  let id = elem.getAttribute("post-id");
  if(!id)
    return;
  xhr_post(c_api_request("api/query&thing=thread&type=view&thingid=1", dptr_thread_container));
  console.log(elem, id);
  e.getAttribute("id");
}
function f_context_onhover(e){
  console.log(e);
}
function f_draw_thread_rows(In_data_object, In_targ_node){
  //let job = JSON.parse(In_data_object._data_out);
  let job = In_data_object.result;
  for(let i = 0; i < job.length; ++i){
    let thread_link = job[i];
    thread_link['str_title'];
    thread_link['uint_cid'];
    thread_link['str_id'];
    thread_link['str_cname'];
    thread_link['uint_pcnt'];
    let dptr= f_generate_thread_link2(thread_link);
    if(dptr < 0)
      continue;
    In_targ_node.parentNode.appendChild(dptr);
  }
}
function f_draw_thread_tree(In_json_in){
  let job = JSON.parse(In_json_in);
  console.log(job);
  for(let i = 0; i < job.length; ++i){
    let comment = job[i];
    if(comment["parent"] >  0){
      //insert as child
      comment["___gen_field_dom" ];
    }
    dptr_com = f_generate_comment(
        comment["author_id"],
        comment["author_name"]);
    dptr_thread_container.appendChild(dptr_com);
    console.log("Comment: " + comment["str_content"]);
  }
  //setAttribute("id", 0);
}

const QUERY_THREADVIEW = 0;
const QUERY_THREAD_TREE = 1;
function f_flairtext(In_level){
  switch(In_level){
    case 0:
      return "[user]";
    case 1:
      return "[administator]";
    case 2:
      return "[moderator]";
  }
}

function f_make_comment(In_node){
  let nid = In_node['node_thread_id'];
  let npid = In_node['node_parent'];
  let realid = parseInt(nid, 32);

  let root = document.createElement("div");
  root.setAttribute("class", "post-container");
  root.setAttribute("data-id", nid);

  let inner = document.createElement("div");
  inner.setAttribute("class", "post")
  inner.setAttribute("pid", In_node['node_thread_id']);
  inner.setAttribute("aid", 1);
  let _flags = In_node["b_uflags"];// |= (0x8|0x10);
  let _level = ((_flags & 0x1C) >> 2);
  let _isclipped = ((_flags & 0x16) >> 4);
  //console.log("Clipped: ", (_isclipped ? "true" : "false"));
  let infobar_top = document.createElement("div");
  infobar_top.setAttribute("class", "infobar");
  infobar_top.appendChild(fast_infolink("", "#" + nid, "Post id"));
  infobar_top.appendChild(
    fast_infolink(
        "user name " + ((_level == 1) ? "user admin " : ""),
        '+' + In_node['author_name'],
        "" + ((_level == 1) ? "[administrator]" : "[user]"))
  );
  //infobar_top.appendChild();
  inner.appendChild(infobar_top);

  let message = document.createElement("div");
  message.setAttribute("class", "post-message-container");
  let message_inner = document.createElement("div");
  message_inner.setAttribute("class", "post-message");
  message.appendChild(message_inner);

  f_node_append_text(message_inner, In_node['str_content']);//.trim(200)/*+ " [id: " + nid + "] [parent: " + npid + "]"/**/);
  inner.appendChild(message);

  infobar_top.appendChild(fast_infolink("action user clickable minimise", "-", "Minimise children"));
  infobar_top.setAttribute("class", "infobar");

  /* Dummy values for design */
  let _vup = getRndInteger(0, 100);
  let _vdown = getRndInteger(0, 25);
  let _vscore = _vup - _vdown;
  // + "[" + _vup + "/" + _vdown + "]"
  infobar_top.insertAdjacentHTML("afterbegin", fast_pill(_vscore));
  infobar_top.insertAdjacentHTML("afterbegin", fast_pill("#1"));
  let infobar_bottom = document.createElement("div");
  infobar_bottom.setAttribute("class", "_infobar");
  infobar_bottom.appendChild(
    fast_infolink(
      "user _action clickable showmore",
      (_isclipped) ? "Show more": "",
      "Show more"
    ));
  if(session.logged_in()){
    infobar_top.appendChild(
      fast_infolink(
          "user action clickable delete",
          'delete',
          "Delete comment [moderator]")
        );
    let _reply = fast_infolink(
      "user action reply clickable", 'reply', "Reply to comment");
    _reply.setAttribute("binding-action-id", "reply");
    _reply.setAttribute("binding-actiontype", "button");
    infobar_top.appendChild(_reply);
  }
  inner.appendChild(infobar_bottom);
  root.appendChild(inner);
  return root;
}
function insert_comment(In_comment_parent_id, In_text){
  xhr_post(
    c_api_request_json(
      {
        url: "api/query&g=createcomment",
        json: {
          "type": "createcomment",
          "args":{
            "threadid":  glob.id,
            "parentid": In_comment_parent_id,
            "text": In_text
          }
        }
      },
      request_callback({ui: dptr_thread_container}, function(e){
        console.log(e.data);
        if(!e.int_result){
          console.log("Insert failed: ", e._data_out);
          alert("Failed to create comment.\nServer returned an error code\nSorry");
          return;
        }
        location.reload();
      }))
  );
}
function rand_insert(__highest_id){
  let int_rand = getRndInteger(1, __highest_id);
  let string = randString(getRndInteger(200, 999));
  insert_comment(int_rand,
    "[Random insert, reply to: " + int_rand + "]" +
    string);
}
function update_thread_tree_binding(e, data){
  let _r = document.getElementById("thread-list");
  if(!_r)
    return;
  let _ui = e; //e.data.ui
  let i = 0;
  let _int_inserted = 0;
  let _int_highest_id = 0;
  let jarray = data.result;
  let rfw = document.createElement("div");
  rfw.setAttribute("class", "reflow-wrapper");
  for(i; i < jarray.length; ++i){
    let _t = _r;
    let node = jarray[i];
    let tn = document.createElement("div");
    if(data.data > 1){ //if root start > 1
      _t = _ui.querySelector("[data-id='" + (data + node['node_parent']) + "']");
    }else if(node['node_parent'] > 1){
      _t = _ui.querySelector("[data-id='" + node['node_parent'] + "']");
      if(!_t)
        _t = rfw;
      else
        _t = _t.parentNode;
    }
    if(node['node_parent'] > _int_highest_id)
      _int_highest_id = node['node_parent']; //debug
    //if(node['depth'] < 2){}
    if(i >= 1)
      tn.setAttribute("class", "tree-node " + ((node['depth'] > 1) ? "indent": ""));
    //else continue;
    let _txt = f_make_comment(node);
    if(i >= 1)
      tn.appendChild(_txt);
    _t.appendChild(tn);
    _int_inserted++;
  }
  _r.appendChild(rfw);
  if(glob.debuginsert && _int_inserted < 200)
    rand_insert(_int_highest_id);
}
function fetch_thread_tree(In_parent_id, __debug_depth_limit){
  if(tempStore.get('tt:' + glob.id)){
    update_thread_tree_binding(dptr_thread_container, tempStore.result());
    return;
  }
  xhr_post(
    c_api_request_json(
      {
        url: "api/query&g=fetchthread",
        json: {
          "type": "threadtree",
          "args":{
            "thingid":  glob.id,
            "depthlimit": __debug_depth_limit
          }
        }
      },
      request_callback(
        { ui: dptr_thread_container, data: In_parent_id}, function(e){
        tempStore.set('tt:' + glob.id, e, 10);
        update_thread_tree_binding(dptr_thread_container, e);
      }
    ))
  );
}
function update_thread_binding(data){
  /*
  //document.title = e.state.pageTitle;
//window.location.hash = '#' + (json['title']).replace(/ |\'|\,/g,"_").trim(50);
//window.location.hash = "#" + strip_url(json['title']);
//json['post_count'] = Number(json['post_count']) + 1000;
  */
  let json = data;
  setTextArea(document.getElementById("thread-title"), json['title']);
  setTextArea(document.getElementById("thread-username-creator"), json['str_username']);
  setTextArea(document.getElementById("thread-date"), json['date_create']);
  setTextArea(document.getElementById("thread-views"), "Views: " + json['int_views']);
  setTextArea(
    document.getElementById("thread-postcount"),//If comments > 1thousand, change the format to 1.0k
    "Comments: " + ((json['post_count'] > 1000) ? ((json['post_count'] / 1000).toFixed(1) + "k") : json['post_count']));
  switch(json['type']){
    case '0':
      setTextArea(document.getElementById("thread-content"), json['content']);
      break;
    case '2':
      document.getElementById("thread-embed-container");
      let src = c_embed_parser.invoke("#thread-embed-container", json['content']);
      //let _frame = e.data.ui.querySelector("#thread-content-embed");
      //_frame.src = json['content'];
      break;
  }
}
function fetch_thread_info(){
  if(tempStore.get('ti:' + glob.id)){
    let _r = tempStore.result();
    return update_thread_binding(_r);
  }
  xhr_post(
    c_api_request_json(
      {
        url: "api/query&g=fetchthreadinfo",
        json: {
          "type": "threadinfo",
          "args":{
            "thingid":  glob.id,
            "thingflags": "0x0"
          }
        },
        data: null
      },
    request_callback({ui: document.getElementById("thread-post-container")},  function(e){
        console.log("Thread get finished");
        if(!e.int_result){
          dptr_thread_container.appendChild(
            f_node_append_text(document.createElement("div"), "Nothing to display"));
          return;
        }
        tempStore.set('ti:' + glob.id, e.result, 10);
        update_thread_binding(e.result);
        console.log(e);
    }))
  );
}
function fetch_sub_info(){
  if(tempStore.get('si:' + glob.id)){
    let _r = tempStore.result();
    return update_thread_binding(_r);
  }
  function info_binding(e){
    /* Set text to relevant sidebar div*/
    setTextArea(
      document.getElementById("sidebar-sub-title"),
      f_amusingTitle(e.result['str_name'])
    );
    document.title += e.result['text_description'];
    setTextArea(
      document.getElementById("sidebar-sub-date"),
      e.result['date_created'] + " (" + f_daydiff(e.result['date_created']) + ") day ago"
    );
    setTextArea(
      document.getElementById("sidebar-sub-description"),
      e.result['text_description']
    );
    setTextArea(
      document.getElementById("sidebar-sub-description"),
      e.result['date_create']
    );
    setTextArea(
      document.getElementById("sidebar-sub-sidebar"),
      e.result['text_sidebar']
    );
  }
  /* Sub info */
  xhr_post(
    c_api_request_json(
      {
        url: "api/query&thing=subinfo",
        json: {
          "type": "subinfo",
          "args" : {
            "thingname": glob.name,
          }
        }
      },
      request_callback({ui: dptr_sidebar_container},function(e){if(!e.int_result) return; info_binding(e); }))
  );
}
function create_comment(In_int_parent, In_str_text){

}
function create_thread(In_str_title){
  xhr_post(
    c_api_request_json(
      {
        url: "api//query&thing=submit",
        json: {
          "type": "createthread",
          "args" : {
          "title": In_str_title,
          "threadtype":  "text",
          //"parentid": 1,
          "threadcontent": "This is an example text post",
          "parentname": glob.name,
          }
        }
      },
    request_callback({ui: dptr_sidebar_container}, function(e){
      if(!e.int_result){
        dptr_thread_container.appendChild(
        f_node_append_text(
          document.createElement("div"),
          "Nothing to display"));
        return;
      }
      if(!has(e.result, "id"))
        return;
      e.result["id"];
      console.log("Finished: ", e.result["id"]);
      location.reload();
    }))
  );
}
function fetch_sub(){
  xhr_post(
    c_api_request_json(
      {
        url: "api/query&thing=threadlist" + glob.name,
        json: {
          "type": "threadlist",
          "args" : {
            "thingname": glob.name,
          }
      }
    },
    request_callback({ui: dptr_thread_container}, function(e){
      if(!e.int_result){
        dptr_thread_container.appendChild(
        f_node_append_text(
          document.createElement("div"),
          "Nothing to display"));
        return;
      }
      console.log("Finished");
        f_draw_thread_rows(e, e.data.ui);
    }))
  );
}
function fetch_front(){
  xhr_post(
    c_api_request_json("api/query&0",
      {
        "type": "sublist",
        "args" : {
          "limit": 20,
        }
    },
      request_callback({ui: dptr_sidebar_container}, function(e){
        console.log("Finished");
        let x = document.getElementById("front-view");
        for(let i = 0; i < e._data_in.length; ++i){
          let root = document.createElement("div");
          root.setAttribute("class", "post-container");
          let inner = document.createElement("div");
          let lvl = e._data_in[i]["access_level"];
          inner.setAttribute("class", "post link-sub padding clickable _highlight-underline "
            + ((lvl > 0) ? "private" : ""));
          inner.setAttribute("sub_nid", e._data_in[i]["name"])
          let message = document.createElement("div");
          message.setAttribute("class", "post-message clickable post-title");
          f_node_append_text(
            message,
            e._data_in[i]["name"]);
            inner.appendChild(message);
            root.appendChild(inner);
            x.appendChild(root);
        }
    }))
  );
}
$('.view').on("click", "div.link-thread, div.link-sub, div.link-comment",
//$('div.link-thread, div.link-comment').click(
  function(e){
    console.log("Link click", e.currentTarget);
    let _pid = e.currentTarget.getAttribute("post-id");
    let _snid = e.currentTarget.getAttribute("sub-nid");
    let url = window.location.href.split('/');
    if(e.currentTarget.classList.contains("link-thread")){
      let query_string = window.location.search;
      let current_url = window.location.href;
      //url[1] + '/' + url[2]+ '/' +
      //window.location = url[3] + '/' + _pid + '/';
      window.location.pathname = url[3] + '/' + _pid + '/';
      //document.location.href = document.location.href + id + '/';
    }else if(e.currentTarget.classList.contains("link-sub")){
      window.location.pathname = '/' + _snid + '/';
    }else{
    }
    return false;
});
function action_handler(){
  let _ob =  {
    _event_table: Array(),
    _callback: function(e){
      let type = e.target.getAttribute('binding-actiontype');
      let flag = e.target.getAttribute("binding-actionflag");
      let id = e.target.getAttribute("binding-action-id");
      let data = e.target.getAttribute("binding-tag-data");
      for(let i = 0; i < this._event_table.length; ++i){
        if((this._event_table[i].type == type)
          && (this._event_table[i].id == id)){
          switch(type){
            case "form":
              if(!e.target.parentNode)
                return; /* If it doesn't have a parent, ignore it*/
              let _x = e.target.closest('div[binding-form=' + id + ']');
              if(!_x)
                return;
              let _f = _x.querySelectorAll("input[binding-data-parent=" + id + "]");
              if(!_f || _f.length == 0)
                return;
              let _s = {};
              for(let _i = 0; _i < _f.length; _i++)
                _s[_f[_i].getAttribute("binding-data-name")] = { value: _f[_i].value, target: _f[_i] };
              this._event_table[i].cb({ id, flag, type, data, target: e.target }, _s);
              return;
              break;
            case "button":
              this._event_table[i].cb({ id, flag, type, data, target: e.target }, {value: data, target: data});
              break;
            case "toggle":
              break;
          }
        }
      }/* End of loop */
      return;
    },
    register: function(In_args){
      if(!has(In_args, "cb") || !has(In_args, "id") || !has(In_args, "type"))
        return;
      this._event_table.push(In_args);
    }
  }
  $(document).on("click", ".action", _ob._callback.bind(_ob));
  return _ob;
}
function session_ui_handler(e){
  console.log("session -> callback -> ui");
  setTextArea(document.getElementById("navbar-account-name"), (e.loggedin ? e.name : "Account"));
  setTextArea(document.getElementById("navbar-account-msgcnt"), e.umsg);
  setTextArea(document.getElementById("navbar-account-login"), (e.loggedin ? "Logout" : "Login"));
}
var reply;
var dptr_thread_container;
var scroller = {
  _x: null,
  _y: null,
  _f: null,
  _limit: 1000,
  _stage: 0,
  _lx: 0,
  init: function(e){
    this._x = document.getElementById('thread-post-container');
    this._limit = $(this._x).offset();
    if(this._limit)
      this._limit = (this._limit.top); //$(window).height());
    this._f = document.createElement('div');
    this._f.style.height = (Math.round(this._limit * 10) / 10) + "px";
    this._f.style.verticalAlign = "top";
    this._f.style.display = "none";
    this._x.parentNode.prepend(this._f);
    return;
  },
  _cb_func: function(e){
    var scroll = $(window).scrollTop();
    //let dir 1; //= (event.originalEvent.wheelDelta >= 0)
    //if(scroll < (this._limit - 10)) return;
    if(!this._stage && (scroll > this._limit)){
      this._y = document.getElementById('sidebar-thread-container');
      this._f.style.display = "block";
      this._y.append(this._x);
      this._stage = true;
    }
    if(this._stage && (scroll < this._limit) && (scroll < this._lx)){
      $(document.getElementById('thread-post-wrapper')).append(this._x);
      this._f.style.display = "none";
      this._stage = false;
    }
    this._lx = scroll;
  }
};
function f_init(){
  if(!session)
    return;
  session.set_callback_handler(function(e){
    session_ui_handler(e);
  });
  session.invoke_callback();

  var handler = action_handler();
  handler.register({
    id: 'login',
    type: 'form',
    cb: function (a, b){
      console.log("form -> login");
      session.login(b.username.value, b.password.value, function(e){
        if(e == 1){
          $('#loginModal').modal('hide');
          $(b.username.target).removeClass("is-invalid");
          $(b.password.target).removeClass("is-invalid");
          return;
        }else{
          $(((e == 3) ? b.username.target : b.password.target)).addClass("is-invalid");
        }
        $(b.username.target).setText("");
        $(b.password.target).setText("");
      });
      /* Potential bug here, change to callback for the call itself */
    }});

  handler.register({
    id: 'open_thread',
    type: 'button',
    cb: function (a, b){

    }
  });
  handler.register({
    id: 'reply',
    type: 'button',
    cb: function (a, b){
      console.log("form -> button");
      $('#replyModal').modal('show');
      let parent = a.target.parentNode.parentNode;
      glob.replyid = parent.getAttribute('pid');
  }});
  handler.register({
    id: 'replyform',
    type: 'form',
    cb: function (a, b){
      console.log("form -> button");
      insert_comment(glob.replyid, b.reply.value);
      reply = b;
  }});
  handler.register({
    id: 'control',
    type: 'button',
    cb: function (a, b){
      switch(a.data){
        case "top":
          $("#thread-top")[0].scrollIntoView({
            behavior: "smooth", // or "auto" or "instant"
            block: "start" // or "end"
          });
          break;
        case "bottom":
          $("#thread-bottom")[0].scrollIntoView({
            behavior: "smooth", // or "auto" or "instant"
            block: "start" // or "end"
          });
          break;
      }
      console.log("form -> control");
  }});
  handler.register({
    id: 'threadlink',
    type: 'button',
    cb: function (a, b){
      console.log("form -> open thread");
      if(!a.target.parentNode)
        return;
      let id =a.target.parentNode.getAttribute("post-id");
      if(!id)
        return;
      window.location.pathname = glob.name + '/' + id + '/';
  }});


  //if(document.location.href[document.location.href.length] != '/')
  //  document.location.href += '/';
  dptr_thread_container = document.getElementById("thread-list");
  dptr_sidebar_container = document.getElementById("sidebar");
  let path = window.location.pathname.split('/');
  ///let _addr_thread = base32.decode(path[2], true);
  if(path[1]){
    glob.name = path[1];
    glob.where = 1;
  }
  if(path[2]){
    ///let _addr_thread = base32.decode(path[2], true);
    glob.id = path[2];// parseInt(path[2], 32)
    $("#navbar-sub-name").text("#" + path[1].trim(20), "...");
    glob.where = 2;
    console.log("Glob: ", glob);
  }
  if(path[3]){
    switch(path[3]){
      case "debug-insert":
      console.log("debug-insert enabled");
      glob.debuginsert = 1;
      break;
      case "depth":
      glob.debug_depth = 2;
      break;
    }
  }
  console.log("Path: ", path);
  ///create_thread("text");
  let _x = document.getElementById("sidebar-sub-input");
  console.log(_x);
  $(document).on("click", ".form_buttom",
    function(e){
      console.log("button click", e.currentTarget);
      let _p = e.currentTarget.parentNode;
      let _nx = _p.querySelectorAll("input");
      let _out = {};
      for(let i = 0; i < _nx.length; ++i){
        /* Experimental, bound to create_thread, rather than generic xhr output*/
        let _f = _nx[i].getAttribute("binding-input");
        if(!isdef(_nx[i].value) || _nx[i].value.length == 0)
          return; //break;
        _out[_f] = _nx[i].value;
      }
      create_thread(_out['title']);
    }
  );
  switch(glob.where){
  case 0:
    fetch_front();
    break;
  case 1:
    fetch_sub();
    fetch_sub_info();
    fast_bakedinput(_x);
    break;
  case 2:
    let _t = document.getElementById("thread-post-container");
    _t.insertAdjacentHTML("afterbegin", fast_thread_header("", "", "", ""));
    _t.insertAdjacentHTML("beforeend", fast_thread_controls());
    let _reply = fast_infolink(
      "user action reply clickable", 'reply', "Reply to comment");
    _reply.setAttribute("binding-action-id", "reply");
    _reply.setAttribute("binding-actiontype", "button")
    fetch_thread_info();
    if(glob.debug_depth)
      fetch_thread_tree(1, glob.debug_depth);
    else
      fetch_thread_tree(1, 100);
    fetch_sub_info();
    break;
  }

  //scroller.init();
  //$(window).on("scroll", scroller._cb_func.bind(scroller));
}
