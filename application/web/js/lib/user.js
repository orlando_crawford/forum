/* This is filled in if the user is logged in*/
var guser = {
  loaded: 0,
  loggedin: 0,
  reason: 0,
  name: "",
  umsg: 0,
}
function type_lookup(In_int){
  let _a = [
    "create",
    "login",
    "logout",
    "sessionok"
  ];
  return _a[In_int];
}
function eraseCookie(name) {
    var d = new Date(); //Create an date object
    d.setTime(d.getTime() - (1000*60*60*24)); //Set the time to the past. 1000 milliseonds = 1 second
    var expires = "expires=" + d.toGMTString(); //Compose the expirartion date
    document.cookie = name+'=; ' + expires + "; path=/"; //"Max-Age=-99999999;';
}
/* Utility functions to read and write cookies */
function setCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
        console.log("Set cookie: %s", name, expires);
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}
function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0)
          return c.substring(nameEQ.length,c.length);
    }
    return null;
}
function c_cookie_controller(){
  return {

  }
}
function c_session_temp(){
  return {
    _last: null,
    _store: null,
    _pack: function(k, ob, expire){
      //t: (expire > 0) ? (new Date((new Date).getTime() + (expire * 10000)) ) : null
      return JSON.stringify({
            o: ob, k: k, t: (expire > 0) ? ((new Date).getTime() + (expire * 1000)) : null
        }
      );
    },
    set : function(str_key, object, timeout_seconds){
      let _o = this._pack(str_key, object, timeout_seconds);
      window.localStorage.setItem(str_key, _o);
    },
    has: function(str_key){
    },
    when: function(str_key){
      window.localStorage.getItem();
    },
    result(){
      console.log("Store: ", this.last);
      return this._last;
      //return this._last.o;
    },
    get: function(str_key){
      let ret = window.localStorage.getItem(str_key);
      if(ret == null)
        return !!(this._last = null); //evaluate the comparison as true, then flip it
        //lol javascript
      let _r = JSON.parse(ret);
      if(!_r || !_r.o || !_r.t)
        return false;
      //console.log("Expire: ", new Date(_r.t));
      //console.log("Now: ", Date.now());
      if((new Date(_r.t)).getTime() < Date.now()){
        console.log("c_session_temp -> expired: (Key: " + _r.k + ")");
        return false;
      }
      this._last = _r.o;
      return true;
    }
  };
}
function controls_scroll(){
  return "";
}
var tempStore = c_session_temp();
function c_user_session(){
  return {
    _cb_func: null,
  _xhr : function(ob){
    if(!ob.type)// || !ob.cb_func || !ob.cb_data)
      return;
    if(!ob.name)
      ob.name = 'unset';
    //let _tn = type_lookup(ob.type);
    //let _tn = ((In_int_type == 0) ? "create" : (In_int_type == 1) ? "login" : "logout");
    let _json = {
      "type": "auth",
      "args":{
        "thingname": type_lookup(ob.type),
      }
    }
    function cb(e){
      /* Collapse nested callback*/
      e.data = e.data.data;
      if(e.data.cb_func)
        e.data.cb_func(e);
    }
    switch(ob.type){
      case 0://"create":
        _json['args']["accountkey"] = ob.passkey;
        _json['args']["accountname"] = ob.name;
        break;
      case 1://"login":
        _json['args']["accountkey"] = ob.passkey;
        _json['args']["accountname"] = ob.name;
        break;
      case 2://"logout":
        break;
      case 3://"sessionok":
        break;
    }
    xhr_post(c_api_request_json(
      {
        url: "api/query&g=session",
        json: _json,
        //{ name: In_str_name, temp_passkey: In_str_password, ref: this },
      },
      request_callback({data: ob}, cb)
      )
    );
  },
  _logout: function(){
    eraseCookie("u_name");
    eraseCookie("u_unread");
    glob.name = 'unset';
    glob.loggedin = false;
    glob.umsg = 0;
    if(this._cb_func)
      this._cb_func(glob);
  },
  set_callback_handler(In_func){
    if(!In_func || !is_function(In_func))
      return;
    this._cb_func = In_func;
    //this._cb_func(glob);
  },
  reason: function(){
    return
  },
  invoke_callback(){
    if(this._cb_func)
      this._cb_func(glob);
  },
  load: function(In_a){
    //if(glob.loaded) return;
    let _cn = getCookie("u_name");
    let _cu = getCookie("u_unread");
    if(_cn && _cu){
      glob.loggedin = true;
      glob.name = _cn;
      glob.umsg = _cu;
      this._xhr({ type: 3, cb_func: function(e){
        if(e.result['response'] == '0')
          this._logout();
      }.bind(this)});
    }else{
      glob.loggedin = false;
    }
    glob.loaded = true;
    if(this._cb_func)
      this._cb_func(glob);
    console.log("session -> loaded");
  },
  login : function(
      In_str_name,
      In_str_password,
      In_cb){
    if(glob.loggedin)
      return;
    this._xhr(
        {
          type: 1,
          name: In_str_name,
          passkey: In_str_password,
          cb_data: In_cb,
          cb_func: function(e){
            console.log("session -> login");
            let response = e.result['response'];
            switch(response){
              case "1":
                glob.loggedin = true;
                glob.name = e.data.name;
                glob.umsg = 100;
                setCookie("u_name", glob.name);
                setCookie("u_unread", glob.umsg);
                e.data.cb_data(1);
                break;
              case "-1":
                glob.loggedin = true;
                e.data.cb_data(2);
                break; /* failed */
              case "-2":
                glob.loggedin = false;
                e.data.cb_data(3);
                break; /*bad account */
              case "-3":
                glob.loggedin = false;
                e.data.cb_data(4);
                break;/* bad passkey */
              default:
                glob.loggedin = false;
                break;
            }
            if(this._cb_func)
              this._cb_func(glob);
        }.bind(this)
    });
  },
  logged_in : function(){
    return glob.loggedin;
  },
  logout: function(In_variable_function_callback){
    //if(!guser.name) return;
    this._xhr({
        type: 2,
        name: glob.name,
        cb_function: function(e){
        this._logout();
        console.log("Logged out");
        //setCookie("u_name", "-1");
      }.bind(this)});
    this._logout();
  },
  create: function(e){}
  }
}
