/*{
  url:
  /* things
  func:
}*/
function request_callback(In_data, In_function){
  return {
    data: In_data,
    func: In_function
  }
}
function c_api_request_json(In_data_object, In_callback_binding){
  if(!has(In_data_object, "json"))
    return null;
  let ret = c_api_request(In_data_object, In_callback_binding);
  ret._data_out = In_data_object.json;
  return ret;
}
function c_api_request(In_request_object, In_callback_data){
    return {
      cb: In_callback_data,
      request: In_request_object,
      time_start: 0,
      time_end: 0,
      int_type: 0,
      str_query: 0,
      int_result: 0,
      _data_out: {},
      _data_in: 0,
    }
}
//      binding: In_ui_binding
/*function c_ui_binding(In_node, In_function){
  return {
    target: In_node,
    func: In_function
  }
}*/
function response_ok_json(In_json_result){
  return (In_json_result['response'] > 0)
}
function xhr_post(In_object){
  In_object.time_start = (new Date).getTime();
  $("#_nav_loader").addClass("loader");
  return $.ajax({
    type: "POST",
    dataType: "json",
    //contentType: "application/json; charset=utf-8",
    data: JSON.stringify(In_object._data_out),
    url: In_object.request.url,
    context: In_object
  //}).always(function(data, textStatus, errorThrown) {
  }).done(function( data, textStatus, jqXHR ) { //replaced depricated success
    $("#_nav_loader").removeClass("loader");
    In_object.time_stop = (new Date).getTime();
    let diff = In_object.time_stop - In_object.time_start;
    console.log("xhr: ok, \n\telapsed: %dms \n\t%dbytes \n\t%dkb \n\t%fmb",
      diff,
      jqXHR.responseText.length,
      jqXHR.responseText.length /1024,
      jqXHR.responseText.length /1024 / 1024);
    console.log(textStatus);
    //api should have returned 400, but returned -1
    if(data.length > 1 && data[0] == '-' && data[1] == '1'){
      console.log("xhr: success returned, but result is bad (-1)");
      return;
    }
    this._data_in = data;
    if(this.cb.func instanceof Function)
      this.cb.func({
        data: this.cb.data,
        result: this._data_in,
        int_result: true
      });
    return true;
  })
  .fail(function(data, jqXHR, textStatus, errorThrown) {
    $("#_nav_loader").removeClass("loader");
    if(data.length > 1 && data[0] == '-' && data[1] == '1')
      return;
    this.int_result = false;
    if(this.cb.func instanceof Function)
      this.cb.func({
        data: this.cb.data,
        result: this._data_in,
        int_result: false
      });
    return false;
  });
}
