var c_embed_parser = {
  _table: [
    {
      p: /(?:http?s?:\/\/)?(?:www\.)?(?:vimeo\.com)\/?(.+)/g,
      r: '//player.vimeo.com/video/$1',
      h: '<iframe width="1280" height="720" src="//player.vimeo.com/video/$1" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>',
      f: 1,
    },
    {
      p: /(?:http?s?:\/\/)?(?:www\.)?(?:youtube\.com|youtu\.be)\/(?:watch\?v=)?(.+)/g,
      r: 'http://www.youtube.com/embed/$1',
      h: '<iframe width="1280" height="720" src="http://www.youtube.com/embed/$1" frameborder="0" allowfullscreen></iframe>',
      f: 1,
    },
    {
      p: /([-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?(?:jpg|jpeg|gif|png))/gi,
      r: '$1',
      h: '<a href="$1" target="_blank"><img class="sml" src="$1"/></a>',
      f: 0,
    }],
  invoke: function(In_target, string){
    $(In_target).append(function(i, html) {
      return c_embed_parser.get_media_container(string);
    }.bind(this));
  },
  get_media_container: function(src){
    for(let i = 0; i < this._table.length; ++i){
      if(this._table[i].p.test(src)){
        //this._table[i].r
        //if(this._table[i].f)
        //  document.createElement();
        return src.replace(this._table[i].p, this._table[i].h);
      }
    }
  }
}
function c_sound(){
  return {
    play : function(In_str_type){
      let _audio = new Audio('/asset/media/sound/' + In_str_type + '.mp3');
      let ret = _audio.play();
      return ret;
    }
  }
}
function supports_html5_storage() {
  try {
    return 'localStorage' in window && window['localStorage'] !== null;
  } catch (e) {
    return false;
  }
}
//_.isFunction = function(obj) {
//  return !!(obj && obj.constructor && obj.call && obj.apply);
//};
function has(ob, key){
  return key in ob;
}
function isdef(In){
  return !(typeof In === "undefined");
}
function is_function(In_var){
  return (In_var instanceof Function)
}
function strip_url(In_string){
  return In_string.replace(/[^a-zA-Z]+/g, '_').trim(60);
}
String.prototype.trim = function(In_int_max, In_suffix){
  if(this.length > 0)
    return this.substring(0, this.length > In_int_max ? In_int_max : this.length) + (((this.length > In_int_max) && In_suffix) ? In_suffix: "");
  return this;
}
/* Insert characters at index of a string Example: myString = myString.insert(2, " Bigger");*/
String.prototype.insert = function (index, string) {
  if (index > 0)
    return this.substring(0, index) + string + this.substring(index, this.length);
  return string + this;
};
function insertAfter(newNode, referenceNode) {
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}
function setTextArea(In_node, In_text){
  if(In_node)
    $(In_node).text(In_text);
}
function getTextArea(In_str_id){
  return document.querySelector("[ui-id= " + In_str_id + "]");
}
function setTextAreas(In_str_id, In_str_text){
  let x = document.querySelector("[ui-text=" + In_str_id + "]");
  for(let i = 0; i < x.length; ++i)
    x.innerHTML = In_str_text;
}
function randString(In_int_length) {
  var text = "";
  var possible = "    ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  for (var i = 0; i < In_int_length; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  return text;
}
function getRndInteger(min, max) {
    return Math.floor(Math.random() * (max - min) ) + min;
}

//Bit slow, change this later to something like copy[0], and concat '/' and the rest of the string
function f_amusingTitle(In_str_text){
  return In_str_text.insert(0, "/").insert(2, "/");
}
function fast_iframe_xs(){
  let _iframe = document.createElement("iframe");
  _iframe.setAttribute("sandbox", "");
  return _iframe;
}
function fast_bakedinput(In_targ){
  In_targ.insertAdjacentHTML("afterbegin",
    `<div id='sidebar-thread-create' class='textFadeIn'>
      <div class='input-container'>
        <div id="post-thread-create">
          <label for='_form_subthread_title'>Thread title</label>
            <input type='text' id='_f_st'  binding-input='title' name='title' placeholder='Title...''>
          <button class='button form_buttom' binding-input='' binding-input-wrapper=''>Create</button>
        </div>
      </div>
    </div>`);
}

function fast_bakedtextarea(In_targ){
  In_targ.insertAdjacentHTML("afterbegin",
    `<div class='input-container' id="input-box-comment">
      <div id="post-thread-create">
        <label for='_form_subthread_title'>Thread title</label>
          <input type='text' id='_f_st'  binding-input='title' name='title' placeholder='Title...''>
        <button class='button form_button' binding-input='' binding-input-wrapper='' binput = '1'>Save</button>
        <button class='button form_button' binding-input='' binding-input-wrapper='' binput = '2'>Cancel</button>
      </div>
    </div>`
  );
}
function fast_thread_header(In_str_heading, In_str_content, In_str_username, In_str_date){
  return `
    <h4 id='thread-title' class="placehold"></h4>
    <div class="embed-container">
      <div id="thread-embed-container" class=" embed-adaptive _embed-responsive _embed-responsive-16by9">
      </div>
    </div>
    <p id='thread-content' class="placehold ph-multiline"></p>
    <div class='infobar thread'>
      <span id='thread-username-creator' class='thread user placehold ph-small'></span>
      <span id='thread-date' class='thread date placehold ph-small'></span>
      <span id='thread-postcount' class='thread date placehold ph-small'></span>
      <span id='thread-views' class='thread date'>%VIEW_COUNT%</span>
    </div>
    <div class="btn-group mt-3 mb-3" role="group" aria-label="Basic example">
      <button type="button" class="btn btn-dark btn-secondary action" binding-actiontype="button" binding-action-id="control" binding-tag-data="top">To Top</button>
      <button type="button" class="btn btn-dark btn-secondary action" binding-actiontype="button" binding-action-id="control" binding-tag-data="bottom">To Bottom</button>
    </div>
  `;
}
function fast_thread_controls(){
  return "<div class='infobar thread'>" + fast_pill("To bottom") + "</div>";
}
function fast_iframe(In_src){
  return "<iframe src="+ In_src + "></iframe>"
}
function fast_embed(In_src){
  return "<embed src="+ In_src + ">";
}
function fast_button(In_text, In_function_cb){
  return "";
}
function fast_pill(Int_str_value){
  return "<span class='badge badge-pill-small badge-secondary _bg-light align-text-bottom'>" + Int_str_value + "</span>";
}
function fast_infolink(In_class, In_text, In_tooltip){
  let info = document.createElement("span");
  info.setAttribute("class", "hoverable " + In_class);
  info.setAttribute("binding-actiontype", In_text);
  info.setAttribute("onclick", "f_context_onclick(this)");
  f_node_set_tooltip(info, In_tooltip);
  f_node_append_text(info, In_text);
  return info;
}
