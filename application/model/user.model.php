<?php
require_once('/framework/core/model.class.php');

class c_m_user extends c_model{
  //define('SOME_VAR', -1);
  private (int) $u8_userflags;
  private (string) $username;
  function deserialize($data_array) : boolean{
    if(!is_array($data_array))
      return false;
    $this->$u8_userflags = $flags[0];
    $this->$username = $flags[1];
    $flags[2];
  }
  function serialize() : Array{
    return Array(
      [0] => (($this->$u8_userflags << 8) | 0x0)
      [1] => ("username"),
      [2] => (0xFF)
    );
  }
  function public set_permission($u8_flags) : boolean{
    return $this->$u8_userflags |= $u8_flags
  }
  function public is_admin() : bool{
    return (
      !($this->$u8_userflags & enum_userflags::USER_LOGGED_IN)
    && $this->$u8_userflags & enum_userflags::USER_ADMIN);
  }
  function public can_access_editor() : bool{
    return is_admin() && $this->$u8_userflags & enum_userflags::USER_ALLOWED_EDITOR;
  }
}
 ?>
