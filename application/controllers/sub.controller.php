 <?php
require_once('/framework/core/controller.class.php');
require_once('/framework/core/view.class.php');

class c_view_thread extends c_view_base{
  function render(){
    return "c_view_thread->rendering";
  }
};

function exists_string($akey){
  return (isset($akey) && is_string($akey));
}
class c_sub_controller extends c_controller{
  public function action(
      string $sub_target,
      string $args = null){

    //echo "<p>c_sub_controller:args:[" . $args . "]</p>";
    //if($args == null)
    //  return 0; //bad call
    //$a_args = explode($args, "/");
    //libxml_use_internal_errors(true);

    $db = new c_db_sub_connection();
    $a_args = preg_split('/[\/]+/', $args, -1, PREG_SPLIT_NO_EMPTY);

    //echo "Strlen: " . $sub_target;
    $strlen = strlen($sub_target);
    if($strlen < 2)
      return;
    if($sub_target[0] == 'r'
      && $sub_target[1] == 'a'){
      switch($sub_target){
        case "rand":
          echo "rand";
          break;
        case "randnsfw":
          break;
        case "randthread":
          break;
        case "randthreadnsfw":
          break;
        default:
          $id = $db->query_sub_exist($sub_target);
          if($id < c_db::QUERY_FAILED || $id != 1)
            return -1;
          break;
      }
    }



    #print_r($a_args);
    //enum_core_error::ERROR_DATABASE_CONNECTION_FAILURE;
    //$db = new c_db_sub_connection();
    //$id = $db->query_sub_id_by_name($sub_target);
    //$info = $db->query_sub_info_by_name($sub_target);

    $cache = new c_memcache_controller();
    $handler = new c_memcache_common_handler();
    if($handler->object_exists($cache, "_c_layout_cache:upper")){
      $layout = $handler->object_get($cache, "_c_layout_cache:upper");
    }else{

    }
    $layout = new c_view_layout_base();
    $layout->set_css([ "lib/bootstrap.min.css", "style.css", "base.css", "sub-animation.css", "controls.css"]);
    $layout->set_footer("text");
    $layout->set_script(
      "window.onload = function(){ __init__(); console.log('window: loaded'); }"
    );
    $layout->set_js([ "jquery/jquery-3.1.0.min.js", "lib/popper.min.js", "lib/bootstrap.min.js",  "base.js", "lib/net.js", "lib/user.js", "lib/util.js", "lib/base32.js", "main.js"]);
    $layout->set_title($sub_target . " | ");

    $loader = new c_view_layout_loader();
    $loader->load_file("application/views/pages/base.shtml");
    if(!$loader->ok())
      return 0;
    $view = new c_view_layout($loader->get());
    //echo "Target: " . $sub_target;
    parse_str($sub_target);
    echo "args: " . $sub_target . " " . $args;
    print_r($args);

    //$view->set_layout_binding($loader->get());
    $view->bind_tag_html("data-ui-subs-sticky",
    "<ol>
      <li><a href='/'>front</a></li>
      <li><a href='/general/'>g/eneral</a></li>
      <li><a href='/dev/'>d/ev</a></li>
      <li><a href='/login/'>Login</a></li>
    </ol>");

    $loader->load_file("application/views/pages/sub.shtml");
    $subview = new c_view_layout($loader->get());
    $subview->bind_tag_html("data-ui-title", "
      <div class='title'>Title</div>
      <div class='description'>Title</div>
      <div class='sub-posts-count'>10000</div>
      ");

    $loader->load_file("application/views/pages/viewflex.shtml");
    $flexview = new c_view_layout($loader->get());
    $flexview->bind_tag_layout("data-view-main", $subview);

    $loader->load_file("application/views/pages/sidebar.shtml");
    $sidebar = new c_view_layout($loader->get());
    /*$sidebar->bind_tag_html("data-sidebar-thread-create-controls","
    <div id='sidebar-sub-input' class='textFadeIn'>
      text3
    </div>");*/
    $flexview->bind_tag_layout("data-view-sidebar", $sidebar);
    //$sidebar->bind_tag_layout("data-view-main", $subview);
    //$view->getAttributeById("")
    $view->bind_tag_layout("data-view-main", $flexview);

    if(is_array($a_args) && count($a_args) > 0 && $a_args[0]){
       $_id = get_id_from_string($a_args[0]);
      //echo "<p>Id:
      //" . $_id . "
      //</p>";
      //$db->query_thread_exists("");
    }
    $thread = new c_view_thread();
    //$view->layout()->bind_tag_view("data-thread-view", $thread);
    //$view->bind_region();
    //$view->draw($layout);
    layout_tree_draw($view, $layout);
    echo "<span>Page loaded in: " . session()->timer()->get_elapsed_ms_now() . "ms</span>";

    //echo "base_convert: " . base_convert(145623, 10, 32);
    //if($info == FALSE)
    //  return false;
    //$ret_array = $db->query_thread_get_tree(1, 1, 0, 0);
    //print_r($ret_array);
    switch($sub_target){
      case 'a':
      break;
    }
  }
}
 ?>
