    <?php
require_once('/framework/core/controller.class.php');
require_once('/framework/lang/interface.python.class.php');
//require_once('../../framework/core/submit.class.php');
function body_ok() : bool{
  return get_body_len() < 256;
}
function get_body_len() : int {
  return $_SERVER['CONTENT_LENGTH'];//filesize ('php://input');
}
function has_body() : bool {
  return filesize ('php://input') > 0;
}
function get_body(){
  return file_get_contents('php://input');
}
function in_requires(array $in, array $keys){
  $len = count($keys);
  for($i = 0; $i < $len;  $i++){
    if(!isset($in[ $keys[$i] ]))
      return false;
  }
  return true;
}
$a_input = [];
function input_args(){
  //return $a_input['args'];
}
function dump_stats(){
  echo "<p>Content:" . $_content_type . "</p>";
  echo "Length: " . get_body_len();
  echo "<p>Input: " . get_body() . "</p>";
}
class enum_api{
  const OK = "1";
  const NO_INPUT = "-1";
  const BAD_INPUT = "-2";
  const BAD_ARGS = "-3";
  const INTERNAL_ERROR = "-4";
}
class enum_api_auth{
  const BAD_PASSWORD = "-5";
  const BAD_USERNAME = "-6";
  const SESSION_IS_ALREADY_LOGGED_IN = "-7";
  const NO_LOGIN = "-8";
}
/* Only used once, pls use api_return_result to enforce standards */
function api_return_result_raw(string $in){
  echo $in;
  http_response_code(200);
  return 1;
}
function api_return_result(array $in){
  //Add response time
  //$in['response_ms'] = session()->timer()->get_elapsed_ms_now();
  $json = json_encode($in);
  if($json == NULL)
    return api_return_error(enum_api::INTERNAL_ERROR);
  print_r($json);
  http_response_code(200);
  return 1;
}
function api_return_error($error){
  echo $error;
  http_response_code(400);
  return -1;
}
class enum_thread_type{
  const TEXT = 0;
  const LINK = 1;
  const LINK_EMBED = 2;
}
class c_controller_api extends c_controller{
  function action(string $str_api_request){
    switch($_SERVER['REQUEST_METHOD']){
      case 'POST':
      break;
      case 'GET':
      break;
    }

    if(isset($_GET)){
      /* This handler will only accept POST requests for security reasons */
      //print_r($_GET);
      //return -1;
    }
    if(isset($_POST)){
      //print_r($_POST);
      //return false;
    }
    if(!isset($_GET['q']))
      return -1;
    //$_content_type = "not found";
    //if(isset($_SERVER["CONTENT_TYPE"]))
    //  $_content_type = $_SERVER["CONTENT_TYPE"];
    $input = get_body();
    $a_input = json_decode($input, true);
    if($a_input == NULL) //could not decode input as json, fail to caller
      return api_return_error(enum_api::BAD_INPUT);
    //print_r($a_input);

    if(!isset($a_input['type']) ||
        !isset($a_input['args']) ||
        !is_array($a_input['args']))
      return api_return_error(enum_api::BAD_ARGS);
    $_in_args = $a_input['args'];
    $_type = $a_input["type"];
    switch($_type){
      case "test":
        $inf = new c_interface_python();
        $inf->execute("", " test ");
        return api_return_result(["response" => "0", "reason" => "test"]);
      break;  
      case "account":

      break;
      case "createthread":{
        $_id = 0;
        $_type = enum_thread_type::TEXT;
        if(!in_requires($a_input['args'], ["title", "threadtype", "threadcontent"]))
          return api_return_error(enum_api::BAD_ARGS);
        //if(isset($_in_args['parentid']) && isset($_in_args['parentname']))
        //  return api_return_error(enum_api::BAD_ARGS);
        if(!session()->user()->is_logged_in())
          return api_return_result(["response" => enum_api_auth::NO_LOGIN, "reason" => "user is not logged in"]);
        switch($_in_args['threadtype']){
          case "text":
            $_type = enum_thread_type::TEXT;
            break;
          case "link":
            $_type = enum_thread_type::LINK;
            if(url_is_embeddable($_in_args['threadcontent']))
              $_type = enum_thread_type::LINK_EMBED;
            break;
          default:
            return api_return_error(enum_api::BAD_ARGS);
            break;
        }
        if(isset($_in_args['parentid'])){
          $_id = $_in_args['parentid'];
          $_id = get_id_from_string($_id);
          if($_id <= 0)
            return api_return_error(enum_api::BAD_ARGS);
        }elseif(isset($_in_args['parentname'])){
          $_id = $_in_args['parentname'];
          $db = new c_db_sub_connection();
          $_id = $db->query_sub_id($_in_args['parentname']);
          if($_id <= 0)
            return api_return_error(enum_api::BAD_ARGS);
        }else{
          return api_return_error(enum_api::BAD_ARGS);
        }
        $_title = $_in_args["title"];
        $_uuid = session()->user()->get_user_id();
        $db = new c_db_sub_connection();
        $ret = $db->query_thread_create($_uuid, 0, $_id, $_title, ".");
        if($ret == false)
          return api_return_error(enum_api::BAD_ARGS);
        $fmt = ["id" => $ret];
        return api_return_result($fmt);
      }break;/* End of createthread */
      case "createcomment":
        $db = new c_db_sub_connection();
        if(!in_requires($a_input['args'], ["text", "parentid", "threadid"]))
          return api_return_error(enum_api::BAD_ARGS);

        $_parent_id = $_in_args['parentid']; //get_id_from_string($_in_args['parentid']); /* Convert base32 string to base10 integer */
        if($_parent_id == 0)
          return api_return_error(enum_api::BAD_ARGS);

        $_thread_id = get_id_from_string($_in_args['threadid']); /* Convert base32 string to base10 integer */
        if($_thread_id == 0)
          return api_return_error(enum_api::BAD_ARGS);

        $ret = $db->query_thread_exists($_thread_id);
        if($ret == false)
          return api_return_error("-10");//enum_api::BAD_ARGS);
        #echo "thread id: " . $_thread_id;
        #echo "parent id: " . $_parent_id;
        if(!session()->user()->is_logged_in()){
          return api_return_result(["response" => enum_api_auth::NO_LOGIN, "reason" => "user is not logged in"]);
          //return api_return_result(["text", "parentid", "threadid"]);
        }
        $_uid = session()->user()->get_user_id();
        $ret = $db->query_comment_insert(
            $_uid,
            $_thread_id,
            $_parent_id,
            $_in_args['text']);
        if($ret == false)
          return api_return_error(enum_api::BAD_ARGS);
        $ret_array = ['ret' => 1];
          return api_return_result($ret_array);
        break; /* end of createcomment */
      case "auth":
        $db = new c_db_user_connection();
        if(isset($_in_args['thingname']))
        switch($_in_args['thingname']){
          case "sessionok":
            if(session()->user()->is_logged_in())
              return api_return_result(["response" => "1"]);
            return api_return_result(["response" => "0"]);
            break;
          case "create":
            if(session()->user()->is_logged_in())
              return api_return_result(["response" => enum_api_auth::SESSION_IS_ALREADY_LOGGED_IN, "reason" => "user is logged in"]);
            if(!in_requires($_in_args, ["accountname", "accountkey"]))
              return api_return_error(enum_api::BAD_ARGS);
            $_name = $_in_args["accountname"];
            $_key = $_in_args["accountkey"];
            if(!is_string($_name) || (strlen($_name) > 10)
              || !is_string($_key) || (strlen($_key) > 20))
              return api_return_result(["response_code" => "-1", "response_message" => "bad password or account name"]);
            $uuid = $db->query_account_exists($_name, $_key);
            if($uuid < 0 || $uuid == 0)
              return api_return_result(["response" => "-2", "reason" => "account exists"]);
            $ret = $db->query_user_create($_key, $_name, "lol@lol.com", 0);
            if($ret <= 0)
              return api_return_error(enum_api::INTERNAL_ERROR);
            if(!session()->user()->set_login($uuid, $_name, $_key))
              return api_return_error(enum_api::INTERNAL_ERROR);
            return api_return_result(["response" => "1", "reason" => "account created"]);
            break;
          case "login":
            /* Store random into in the db with the session, and store it in the session key, check this when trying to do admin actions */
            $_db_uuid = random_int (1 , 0xFFFFFFFF ); //4byte integer
            $_userid = $_db_uuid ^ session()->client_address();
            if(session()->user()->is_logged_in())
              return api_return_result(["response" => "-1", "reason" => "user is logged in"]);
            if(!in_requires($_in_args, ["accountname", "accountkey"]))
              return api_return_error(enum_api::BAD_ARGS);
            $_name = $_in_args["accountname"];
            $_key = $_in_args["accountkey"];
            if(!is_string($_name) || (strlen($_name) > 10)
              || !is_string($_key) || (strlen($_key) > 20))
              return api_return_error(enum_api::BAD_ARGS);
            $uuid = $db->query_account_exists($_name, $_key);
            if($uuid < 0) /* error */
              return api_return_error(enum_api::INTERNAL_ERROR);
            if($uuid == 0) /* does not exist */
              return api_return_result(["response" => "-2", "reason" => "bad account"]);
            $ret = $db->query_account_keyhash($uuid);
            if(!is_string($ret) && $ret < 0)
              return api_return_error(enum_api::INTERNAL_ERROR);
            if(!password_verify($_key, $ret)) /* Compare user submitted key against the stored hash for the account*/
              return api_return_result(["response" => "-3", "reason" => "bad passkey"]);
            //$ret = $db->query_account_info_($uuid);

            if(!session()->user()->set_login($uuid, $_name, $_key))
              return api_return_error(enum_api::INTERNAL_ERROR);
            return api_return_result(["response" => "1", "reason" => "logged in", "info" => "test"]);
            break;
          case "logout":
            //if(!in_requires($_in_args, ["accountkey" ]))
            //  return api_return_error(enum_api::BAD_ARGS);
            //$_key = $_in_args["accountkey"];
            session()->user()->set_logout();
            session()->user()->get_user_id();
            return api_return_result(["response" => "1", "reason" => "logged out"]);
            break;
        }
        //$_id = $_in_args['thingname'];
  break;
      case "rand":
        $_id = $_in_args['thingname'];
        switch($_id){
          case "sub":
            $_id = $db->query_rand_thing(enum_DB_RAND_SORT::RANDOM_SUB);
            if($_id <= 0) //shouldn't happen at all
              return api_return_error(enum_api::INTERNAL_ERROR);
            /* Return subname, subid*/
            break;
          case "thread":
            /* Return subname, subid and threadid */
            $_id = $db->query_rand_thing(enum_DB_RAND_SORT::RANDOM_THREAD);
            if($_id <= 0) //shouldn't happen at all
              return api_return_error(enum_api::INTERNAL_ERROR);
            break;
          default:
            return api_return_error(enum_api::BAD_ARGS);
            break;
        }
        break;
      case "subinfo":
        $db = new c_db_sub_connection();
        $_id = 0;
        if(!in_requires($a_input['args'], ["thingname"]))
          return api_return_error(enum_api::BAD_ARGS);
        $_id = $_in_args['thingname'];
        $_id = $db->query_sub_id($_id);
        if($_id <= 0)
          return api_return_error(enum_api::BAD_ARGS);
        $ret_array = $db->query_sub_info($_id);
        if($ret_array == false)
          return api_return_error(enum_api::BAD_ARGS);

        //$inf = new c_interface_python();
        //$inf->execute("", "");

        return api_return_result($ret_array);
      break;/* End of subinfo */
      case "threadinfo":
        $db = new c_db_sub_connection();
        $_id = 0;
        if(!in_requires($_in_args, ['thingid']))
          return api_return_error(enum_api::BAD_ARGS);
        $_id = get_id_from_string($_in_args['thingid']);
        if($_id <= 0)
          return api_return_error(enum_api::BAD_ARGS);
        if(!$db->query_thread_info($_id))
          return api_return_error(enum_api::BAD_ARGS);
        $ret = $db->get_result();
        return api_return_result($ret);
        break;/* End of threadinfo */
      case "threadtree":
        $db = new c_db_sub_connection();
        $_id = 0;
        if(!in_requires($_in_args, ["thingid"]))
          return api_return_error(enum_api::BAD_ARGS);
        $_id = get_id_from_string($_in_args['thingid']);
        if($_id <= 0)
          return api_return_error(enum_api::BAD_ARGS);
        $_parent_id = 1;
        if(isset($_in_args['commentparentid'])){
          $_parent_id = get_id_from_string($_in_args['commentparentid']);
          if($_parent_id == 0)
            return api_return_error(enum_api::BAD_ARGS);
        }
        $_maxdepth = 3;
        if(isset($_in_args['depthlimit'])){
          $_maxdepth = $_in_args['depthlimit'];
        }
        $_limit = 100;
        if(isset($_In_args['limit'])){
          if(is_int($_limit))
            $_limit = $_in_args['limit']; /* limit number of comments to return from the parentid; */
        }
        $ret_array = $db->query_thread_get_tree(
          $_id,
          $_parent_id,
          $_maxdepth);
        if(!$ret_array)
          return api_return_error("-12");//enum_api::BAD_ARGS);
        return api_return_result_raw($ret_array);
        break;/* End of threadtree */
      case "threadlist":
        $db = new c_db_sub_connection();
        $_id = 0;
        if(!in_requires($a_input['args'], ["thingname"]))
          return api_return_error(enum_api::BAD_ARGS);
        $_id = $_in_args['thingname'];

        switch($_id){
          case "front":
            /* frontpage is a special case */
            break;
          default:
            $_id = $db->query_sub_id($_id);
            break;
        }
        if($_id <= 0)
          return api_return_error(enum_api::BAD_ARGS);
        $ret_array = $db->query_thread_list(
           $_id,
           c_thread_sorting::NEW,
           time());
        if($ret_array == false)
          break;
        return api_return_result($ret_array);
      break; /* End of threadlist */
      case "sublist":
        $db = new c_db_sub_connection();
        /* request sub list */
        $_lim = -1;
        if(!in_requires($a_input['args'], ["filtername"]))
          return api_return_error(enum_api::BAD_ARGS);
        if(isset($_in_args['limit']))
          $_lim = $_in_args['limit'];
        $ret_array = $db->query_sub_list(-1);
        //print_r($ret_array);
        if($ret_array == false)
          break;
        return api_return_result($ret_array);
      break;
    }
    return api_return_error(enum_api::NO_INPUT);
  }
}
?>
