<?php
  require_once('/framework/core/controller.class.php');
  require_once('/framework/core/view.class.php');

  class enum_basepage{
    const ERROR = 'error';
    const HOME = 'home';
  }
  class c_basepage_controller extends c_controller {
    public function action(string $page){
      $loader = new c_view_layout_loader();
      $loader->load_file("application/views/pages/base.shtml");

      $layout = new c_view_layout_base();
      //$layout->set(html_layout_top(),html_layout_bottom("tag", "stats"));
      $layout->set_css(["base.css", "link.css"]);
      $layout->set_title("Bigbox php base");
      $layout->set_footer("text");
      $layout->set_js(["base.js", "lib/net.js", "lib/user.js", "lib/util.js", "main.js", "lib/base32.js",   "jquery/jquery-3.1.0.min.js"]);
      $layout->set_script(
        "window.onload = function(){ __init__(); console.log('window: loaded'); }"
      );
      $loader = new c_view_layout_loader();
      $loader->load_file("application/views/pages/base.shtml");
      if(!$loader->ok())
        return 0;
      $view = new c_view_layout($loader->get());
      switch($page){
        case enum_basepage::ERROR:
          break;
        case enum_basepage::HOME:
          $view->bind_tag_html("data-ui-subs-sticky",
          "<ol>
            <li><a href='/general/'>g/eneral</a></li>
            <li><a href='/dev/'>d/ev</a></li>
            <li><a href='/login/'>Login</a></li>
          </ol>");

          $loader->load_file("application/views/pages/viewflex.shtml");
          $flexview = new c_view_layout($loader->get());

          $loader->load_file("application/views/pages/sidebar.shtml");
          $sidebar = new c_view_layout($loader->get());
          $sidebar->bind_tag_html("data-sidebar-thread-create-controls","
          <div id='sidebar-sub-input' class='textFadeIn'>
            text3
          </div>");
          $flexview->bind_tag_layout("data-view-sidebar", $sidebar);

          $loader->load_file("application/views/pages/frontpage.shtml");
          $frontpage = new c_view_layout($loader->get());
          $flexview->bind_tag_layout("data-view-main", $frontpage);
          $view->bind_tag_layout("data-view-main", $flexview);

          layout_tree_draw($view, $layout);
          //$view = new c_view_base();
          //$view = new c_view_home();
          //$view->set_layout_binding($loader->get());
          //$view->layout()->bind_tag_view();          //$view->bind_region();
          //$view->draw($layout);
          break;
        default:
          return -1;
          break;
      }
      return 0;
      /* end */
    }
  }
?>
