<?php
  //echo phpinfo();
  //ini_set('include_path', get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT'] . "/forum/" );
  require_once('/framework/core/session.php');
  //session()->cookie_check();
  /*$use_sts = false;
  // HTTPS to 'off' for non-SSL requests
  if ($use_sts && isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {
    header('Strict-Transport-Security: max-age=31536000');
  } elseif ($use_sts) {
    header('Location: https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'], true, 301);
    // we are in cleartext at the moment, prevent further execution and output
    die();
  }*/
  //for($i =0; $i < 1000; $i++) echo "<p>" . base_convert($i, 10, 32) ."</p>";
  if (count($_GET) > 0
    && isset($_GET['action'])
    && isset($_GET['intent'])) {
      //$g_controller =   'page';
    $g_site =         $_GET['action'];
    $g_action     =   $_GET['intent'];
    if(isset($_GET['index']))
      $g_index = $_GET['index'];
    else
      $g_index = "";
  } else {
    echo 'no args';
    $g_controller = 'pages';  //Redirect the user to /default non-scary pages
    $g_site =       'index';   //note, disallow this from being generated
    $g_action     = 'home';
    $g_page_start = 0;
    $g_index = '';
  }

  //ABORT_ERROR("Session failed to init, if this persists please contact the webmaster", 4);
  /* Load session */
  $ret = session()->init();
  if($ret < 0)
    ABORT_ERROR("Session failed to init, if this persists please contact the webmaster", 2);
  //session()->parse();
  //session()->intent()->controller();

  //declare(strict_types=1);
  require_once('/framework/database/database.class.php');
  require_once('/framework/lib/util.php');
  require_once('/framework/core/router.class.php');
  require_once('/framework/core/input.class.php');

  $timer = new c_timer();
  $timer->start();
  //(new c_memcache_controller())->get_instance();
  //_daprint($_GET);

  $db = new c_db_stats_connection();
  $int_sessions = $db->query_get_num_active_session();
  dlog()->line(3, '[STATS] active sessions (timeout after 10 minutes) = ' . $int_sessions);
    //echo session()->user()->is_admin() ? 'Admin' : 'Not admin';

  $db = new c_db_sub_connection();
  //$db->query_sub_exist("root");
  $db->query_sub_create("root");
  //'INSERT INTO forum.thread_comment (content, parent, rank) VALUES('text', 2, 0);'

  /*$db = new c_db_sub_connection();
  $db->query_site_info($g_site);
  $db->query_site_exist($g_site);
  */
  $db = new c_db_user_connection();
  //$db->query_account_exists('root', 'hash');
  //$db->query_user_create("test", "test", "test", 0);
  //session()->user()->login('root' , 'hashkeylol');
  //echo create_input()->render();
  if(router()->navigate($g_site, $g_action, $g_index) < 0)
    return router()->navigate_root();
  /*Import layout*/

  $timer->stop();

  //echo "Page generated in: " . $timer->get_elapsed_ms() . "ms";
  //echo "The target average: < 100ms";
  dlog()->line(3, 'end');
?>
